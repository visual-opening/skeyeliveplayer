## SkeyeLivePlayer-Win #

SkeyeLivePlayer **Windows** 播放器是由[OpenSKEYE开放平台](http://www.openskeye.cn "OPENSKEYE视频云")开发和维护的一个完善的RTSP/RTMP/SKEYELIVE流媒体播放器项目，视频编码支持**H.264**，**H.265**，**MPEG4**，**MJPEG**，音频支持**G711A/U**，**PCM**，**G726**，**AAC**，支持RTSP over TCP/UDP协议，以及SKEYELIVE over KCP/TCP/RDP；支持**软/硬解码**，是一套极佳的安防流媒体平台播放组件！SkeyeLivePlayer Windows版本经过了很多年的发展和迭代，已经非常稳定、完整，现已开源大部分（80%）代码，方便大家在项目中集成以及二次开发，功能包括：直播、录像、抓图，应该说是目前市面上功能性、稳定性和完整性最强的一款流媒体播放器！

> 国内大部分的RTSP/RTP协议的播放器都是基于ffmpeg做的(包括协议层和解码层)，但是在实际的RTSP/RTP项目实战过程中，由于各个厂家的自定义字段的不同，由诸多需要特殊兼容的地方，而这正是ffmpeg类播放器无法做到的，SkeyeLivePlayer已经将国内几乎所有的摄像机厂家都适配了一遍，非常兼容，非常稳定！
> 
> skeyelive协议，是OpenSKEYE视频云团队开发的一款高效率的流媒体分发协议，具有高效率、高并发、低延迟、低资源占用率等特点，目前支持底层通过Kcp和Tcp协议传输，单台支持200路以上的并发。


## 功能清单 

1. 超低延迟的SkeyeLive、RTSP、RTMP协议流媒体直播播放器；

2. 超强的设备兼容性和可定制性；

3. 完美支持多窗口多实例播放；

4. 支持RTSP OVER TCP/UDP模式切换；

5. 支持播放端Buffer实时设置，成熟的低延时追帧技术；

6. 秒开播放功能；

7. 支持自定义播放布局;

8. 支持nvidia硬件解码和软解码、D3D/GDI渲染切换显示、播放，更加灵活;

9. 支持OSD文字水印；

10. 支持实时录像、jpg/Png抓图快照;

11. 支持按比例显示或铺满窗口;

12. 详尽的log日志输出回调；



## 最新版本下载 ##

- Windows RTSP播放器：[https://gitee.com/visual-opening/skeyeliveplayer/tree/master/bin_x64](https://gitee.com/visual-opening/skeyeliveplayer/tree/master/bin_x64)

- Windows SkeyeLive同屏开发组件：[https://gitee.com/visual-opening/skeyelive](https://gitee.com/visual-opening/skeyelive)


## DEMO效果 ##

![SkeyeLivePlayer Win](./img/skeyeliveplayer.png)




## 技术支持 ##

- 邮件：[support@openskeye.cn](mailto:support@openskeye.cn) 

- QQ交流群：<a href="https://jq.qq.com/?_wv=1027&k=OIg9y2QB" target="_blank" title="SkeyePlayer">**102644504**</a>

- OpenSKEYE官网: http://www.openskeye.cn/

- 我们同时提供 Windows、Android、Linux 版本的 SkeyeLive 同屏技术。

- SkeyeLivePlayer 免费提供个人学习使用，商业使用需要经过授权才能永久使用，商业授权方案可以通过以上渠道进行更深入的技术与合作咨询。



