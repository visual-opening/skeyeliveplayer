/*
	Copyright (c) 2018-2023 OpenSKEYE.CN.  All rights reserved.
	Github: https://gitee.com/visual-opening/skeyeplayer
	WEChat: OpenSKEYE
	Website: http://www.OpenSKEYE.CN
	Author: Sword@OpenSKEYE.CN
*/
#include "libSkeyePlayerAPI.h"
#include "ChannelManager.h"

CChannelManager	*g_pChannelManager = NULL;

#define KEY "6D75724D7A4969576B5A7341724A5A696E576C54346674305A584E303856634D5671442F3465424859585A7062695A4359574A76633246414D6A41784E6B566863336C4559584A33615735555A5746745A57467A65513D3D"

// 初始化SDK
LIB_SkeyePLAYER_API int SkeyePlayer_Init(char* key)
{

	SkeyeLivePlayer::initializePlayer(0,NULL);

	int isSkeyeRTSPClientActivated = SkeyeRTSP_Activate(key);
	switch(isSkeyeRTSPClientActivated)
	{
	case SKEYE_ACTIVATE_INVALID_KEY:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_INVALID_KEY!\n");
		break;
	case SKEYE_ACTIVATE_TIME_ERR:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_TIME_ERR!\n");
		break;
	case SKEYE_ACTIVATE_PROCESS_NAME_LEN_ERR:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_PROCESS_NAME_LEN_ERR!\n");
		break;
	case SKEYE_ACTIVATE_PROCESS_NAME_ERR:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_PROCESS_NAME_ERR!\n");
		break;
	case SKEYE_ACTIVATE_VALIDITY_PERIOD_ERR:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_VALIDITY_PERIOD_ERR!\n");
		break;
	case SKEYE_ACTIVATE_SUCCESS:
		printf("SkeyeRTSPClient_KEY is SKEYE_ACTIVATE_SUCCESS!\n");
		break;
	}

	if(SKEYE_ACTIVATE_SUCCESS >= isSkeyeRTSPClientActivated)
		return -1;

	if (NULL == g_pChannelManager)
	{
		g_pChannelManager = new CChannelManager();
		g_pChannelManager->Initial();
	}

	if (NULL == g_pChannelManager)		return -1;

	return isSkeyeRTSPClientActivated;
}

// Release
LIB_SkeyePLAYER_API void SkeyePlayer_Release()
{
	if (NULL != g_pChannelManager)
	{
		delete g_pChannelManager;
		g_pChannelManager = NULL;
	}
}

LIB_SkeyePLAYER_API int SkeyePlayer_OpenStream(const char *url, HWND hWnd, RENDER_FORMAT renderFormat, int rtpovertcp,
	const char *username, const char *password, MediaSourceCallBack callback, void *userPtr, char* startTime, char* endTime, float fScale, int decodeType, bool bInnerShow)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->OpenStream(url, hWnd, renderFormat, rtpovertcp, username, password, callback, userPtr, startTime, endTime, fScale, decodeType, bInnerShow);
}

LIB_SkeyePLAYER_API void SkeyePlayer_CloseStream(int channelId)
{
	if (NULL == g_pChannelManager)		return;

	g_pChannelManager->CloseStream(channelId);
}

/*  设置播放速度 */
LIB_SkeyePLAYER_API int SkeyePlayer_SetStreamSpeed(int channelId, float fSpeed)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetStreamSpeed(channelId, fSpeed);

}
/* 跳转播放时间 */
LIB_SkeyePLAYER_API int SkeyePlayer_SeekStream(int channelId, const char *playTime)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->SeekStream(channelId, playTime);
}
/* 暂停 */
LIB_SkeyePLAYER_API int SkeyePlayer_PauseStream(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->PauseStream(channelId);
}
/* 恢复播放 */
LIB_SkeyePLAYER_API int SkeyePlayer_ResumeStream(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->ResumeStream(channelId);
}


LIB_SkeyePLAYER_API int SkeyePlayer_SetFrameCache(int channelId, int cache)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetFrameCache(channelId, cache);
}
LIB_SkeyePLAYER_API int SkeyePlayer_SetShownToScale(int channelId, int shownToScale)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetShownToScale(channelId, shownToScale);
}

LIB_SkeyePLAYER_API int SkeyePlayer_SetDecodeType(int channelId, int decodeKeyframeOnly)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetDecodeType(channelId, decodeKeyframeOnly);
}
LIB_SkeyePLAYER_API int SkeyePlayer_SetRenderRect(int channelId, LPRECT lpSrcRect)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetRenderRect(channelId, lpSrcRect);
}

LIB_SkeyePLAYER_API int SkeyePlayer_ShowStatisticalInfo(int channelId, int show)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->ShowStatisticalInfo(channelId, show);
}

LIB_SkeyePLAYER_API int SkeyePlayer_ShowOSD(int channelId, int show, SKEYE_PALYER_OSD osd)
{

	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->ShowOSD(channelId, show, osd);
}


LIB_SkeyePLAYER_API int SkeyePlayer_SetDragStartPoint(int channelId, POINT pt)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetDragStartPoint(channelId, pt);
}
LIB_SkeyePLAYER_API int SkeyePlayer_SetDragEndPoint(int channelId, POINT pt)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetDragEndPoint(channelId, pt);
}
LIB_SkeyePLAYER_API int SkeyePlayer_ResetDragPoint(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->ResetDragPoint(channelId);
}


LIB_SkeyePLAYER_API int SkeyePlayer_PlaySound(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->PlaySound(channelId);
}
LIB_SkeyePLAYER_API int SkeyePlayer_StopSound()
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->StopSound();
}

LIB_SkeyePLAYER_API int SkeyePlayer_StartManuRecording(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->StartManuRecording(channelId);
}

LIB_SkeyePLAYER_API int SkeyePlayer_StopManuRecording(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->StopManuRecording(channelId);
}

LIB_SkeyePLAYER_API int		SkeyePlayer_SetManuRecordPath(int channelId, const char* recordPath)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetManuRecordPath(channelId, recordPath);
}

LIB_SkeyePLAYER_API int		SkeyePlayer_SetManuPicShotPath(int channelId, const char* shotPath)
{
	if (NULL == g_pChannelManager)		return -1;

	return g_pChannelManager->SetManuPicShotPath(channelId, shotPath);
}

LIB_SkeyePLAYER_API int		SkeyePlayer_StartManuPicShot(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->StartManuPicShot(channelId);
}

LIB_SkeyePLAYER_API int		SkeyePlayer_StopManuPicShot(int channelId)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->StopManuPicShot(channelId);
}

LIB_SkeyePLAYER_API int		SkeyePlayer_AddBuoy(int channelId, D3D9_BUOY buoy)
{
	if (NULL == g_pChannelManager)		return -1;
	return g_pChannelManager->AddBuoy(channelId, buoy);

}

LIB_SkeyePLAYER_API int SkeyePlayer_DoMouseEvent(int channelId, Render_MouseEventType eType, RECT rcWnd, char* sGetValue)
{
	int ret = -1;
	if (NULL == g_pChannelManager)		
		return ret;
	string sValue;
	ret = g_pChannelManager->DoMouseEvent(channelId,eType, rcWnd, sGetValue);
	return ret;
}