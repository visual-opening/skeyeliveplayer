#ifndef NVDECODERAPI_H
#define NVDECODERAPI_H

#include <string>

//++ typedefine start
#ifndef NVDECODER_HANDLE
#define NVDECODER_HANDLE void*
#endif//NVDECODER_HANDLE

typedef enum _OutputFormat //native=默认解码器输出为NV12格式
{
	native = 0, bgrp, rgbp, bgra, rgba, bgra64, rgba64
}OutputFormat;


typedef enum _NvDecoder_CodecType {
	NvDecoder_Codec_MPEG1 = 0,                                         /**<  MPEG1             */
	NvDecoder_Codec_MPEG2,                                           /**<  MPEG2             */
	NvDecoder_Codec_MPEG4,                                           /**<  MPEG4             */
	NvDecoder_Codec_VC1,                                             /**<  VC1               */
	NvDecoder_Codec_H264,                                            /**<  H264              */
	NvDecoder_Codec_JPEG,                                            /**<  JPEG              */
	NvDecoder_Codec_H264_SVC,                                        /**<  H264-SVC          */
	NvDecoder_Codec_H264_MVC,                                        /**<  H264-MVC          */
	NvDecoder_Codec_HEVC,                                            /**<  HEVC              */
	NvDecoder_Codec_VP8,                                             /**<  VP8               */
	NvDecoder_Codec_VP9,                                             /**<  VP9               */
	NvDecoder_Codec_NumCodecs,                                       /**<  Max codecs        */
} NvDecoder_CodecType;

typedef enum _NvDecoder_YUVType {

	// Uncompressed YUV
	NvDecoder_YUV420 = (('I' << 24) | ('Y' << 16) | ('U' << 8) | ('V')),   /**< Y,U,V (4:2:0)      */
	NvDecoder_YV12 = (('Y' << 24) | ('V' << 16) | ('1' << 8) | ('2')),   /**< Y,V,U (4:2:0)      */
	NvDecoder_NV12 = (('N' << 24) | ('V' << 16) | ('1' << 8) | ('2')),   /**< Y,UV  (4:2:0)      */
	NvDecoder_YUYV = (('Y' << 24) | ('U' << 16) | ('Y' << 8) | ('V')),   /**< YUYV/YUY2 (4:2:2)  */
	NvDecoder_UYVY = (('U' << 24) | ('Y' << 16) | ('V' << 8) | ('Y'))    /**< UYVY (4:2:2)       */
} NvDecoder_YUVType;

#ifdef __cplusplus
extern "C"
{
#endif

	int NvDecoder_Initsize(std::string &erroStr);

	//除非使用低延迟模式，否则请不要使用此标志bLowLatency，但是使用此标志很难获得硬件解码器100%的利用率。
	NVDECODER_HANDLE NvDecoder_Create(NvDecoder_CodecType codec, int videoW, int videoH, bool bLowLatency, bool bUseGPUMemory, OutputFormat eOutputFormat, int& errCode, std::string &erroStr);
	int NvDecoder_Decode(NVDECODER_HANDLE handle, const uint8_t *pData, int nSize, uint8_t ***pppFrame, int* pnFrameLen, int *pnFrameReturned);
	int NvDecoder_DecodeHW(NVDECODER_HANDLE handle, const uint8_t* pData, int nSize, uint8_t*** pppFrame, int* pnFrameLen, int* pnFrameReturned);
	void NvDecoder_Release(NVDECODER_HANDLE handle);

	int NvDecoder_Uninitsize();

	typedef NVDECODER_HANDLE(*funcNvDecoder_Create)(NvDecoder_CodecType codec, int videoW, int videoH, bool bLowLatency, bool bUseGPUMemory, OutputFormat eOutputFormat, int& errCode, std::string &erroStr); // 宏定义函数指针类型
	typedef int(*funcNvDecoder_Decode)(NVDECODER_HANDLE handle, const uint8_t *pData, int nSize, uint8_t ***pppFrame, int* pnFrameLen, int *pnFrameReturned);
	typedef int(*funcNvDecoder_DecodeHW)(NVDECODER_HANDLE handle, const uint8_t *pData, int nSize, uint8_t ***pppFrame, int* pnFrameLen, int *pnFrameReturned);
	typedef void(*funcNvDecoder_Release)(NVDECODER_HANDLE handle);

#ifdef __cplusplus
}
#endif

#endif // NVDECODERAPI_H
