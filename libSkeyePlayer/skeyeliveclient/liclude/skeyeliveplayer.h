﻿#ifndef SKEYELIVEPLAYER_H
#define SKEYELIVEPLAYER_H

#include "libskeyeliveplayer_global.h"

#include <string>

namespace skeye_live {

struct FrameInfo
{
    enum class VideoCodec : uint8_t {
        Encode_H264, /*!< H264 */
        Encode_H265  /*!< H265 */
    };

    enum class AudioCodec : uint8_t {
        Encode_PCM, /*!< 原始PCM */
        Encode_AAC  /*!< AAC */
    };

    union {
        struct VideoInfo {
            VideoCodec codec; /*!< 视频编码 */
            int width;        /*!< 视频宽度 */
            int height;       /*!< 视频高度 */
            int fps;          /*!< 视频帧率 */
        } _video;
        struct AudioInfo {
            AudioCodec codec; /*!< 音频编码 */
            int channels;     /*!< 音频通道数 */
            int sampleSize;   /*!< 音频样本大小(位) */
            int sampleRate;   /*!< 音频采样率 */
        } _audio;
    } _info;

    uint64_t captureTimestamp = 0; /*!< CTS采集时间戳 */
    uint64_t decodeTimestamp = 0;  /*!< DTS解码时间戳 */

    void clear() { memset(&_info, 0, sizeof (_info)); }
};

class SkeyeLivePlayerPrivate;
class LIBSKEYELIVEPLAYER_EXPORT SkeyeLivePlayer
{
public:
    enum class CallbackType {
        VideoFrame,       /*!< 视频帧 */
        AudioInputFrame,  /*!< 音频输入帧 */
        AudioOutputFrame  /*!< 音频输出帧 */
    };

    enum class VideoPixelFormat {
        Format_RGBA32, /*!< RGBA 32 */
        Format_YUV420P /*!< YUV420P */
    };

    enum class ConnectionType {
        Type_Tcp, /*!< TCP连接 */
        Type_Kcp, /*!< KCP连接 */
        Type_Rdp  /*!< RDP连接 */
    };

    enum class DecoderType {
        Type_Software, /*!< 软编码 */
        Type_Nvidia    /*!< Nvidia硬编码 */
    };

    typedef int (*player_callback)(CallbackType type, uint64_t data, uint32_t dataSize, FrameInfo *frameInfo, void *userPtr);

    /**
     * @brief 创建SkeyeLivePlayer实例
     * @param type 连接类型{@link ConnectionType @endlink}
     * @param decoderType 解码器类型{@link DecoderType @endlink}
     */
    SkeyeLivePlayer(ConnectionType type = ConnectionType::Type_Tcp, DecoderType decoderType = DecoderType::Type_Software);
    ~SkeyeLivePlayer();

    /**
     * @brief 初始化播放器
     * @warning 非Qt环境下需要在创建实例前调用
     * @param argc main参数,可以为0
     * @param argv main参数,可以为nullptr
     */
    static void initializePlayer(int argc = 0, char *argv[] = nullptr);

    /**
     * @brief 开启Skeyelive连接
     * @param liveStream Skeyelive流地址{skeyelive://}开头
     * @param pixelFormat 输出视频像素格式{@link VideoPixelFormat @endlink}
     * @param callback 码流数据回调(decode==true时为解码数据,否则为编码数据)
     * @param userPtr 码流回调指针
     * @param decode 是否解码
     */
    void startLiveConnection(const std::string &liveStream, VideoPixelFormat pixelFormat, player_callback callback, void *userPtr, bool decode = true);

    /**
     * @brief 停止Skeyelive连接
     */
    void stopLiveConnection();

private:
    SkeyeLivePlayerPrivate *d = nullptr;
};

};

#endif // SKEYELIVEPLAYER_H
