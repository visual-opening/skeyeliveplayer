#ifndef __FF_CODEC_API_H__
#define __FF_CODEC_API_H__

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define FFCODEC_API  __declspec(dllexport)
//=======================================================
#define CODEC_NONE			0x00			//28

//Decoder
#ifndef CODEC_H264
#define CODEC_H264			0x1B			//27
#endif
#ifndef CODEC_H265
#define CODEC_H265			0xAD			//173
#endif
#ifndef CODEC_MPEG4
#define CODEC_MPEG4			0x0D			//13
#endif
#ifndef CODEC_MPEG2
#define CODEC_MPEG2			0x02			//2
#endif
#ifndef CODEC_MJPEG
#define CODEC_MJPEG			0x08			//8
#endif

#ifndef CODEC_MP3
#define CODEC_MP3				0x15001			//86017
#endif

#ifndef CODEC_AAC
#define CODEC_AAC				0x15002			//86018
#endif

//=======================================================
//输出格式
#ifndef OUTPUT_PIX_FMT_YUV420P
#define OUTPUT_PIX_FMT_YUV420P		0
#endif
#ifndef OUTPUT_PIX_FMT_YUYV422
#define OUTPUT_PIX_FMT_YUYV422		1
#endif
#ifndef OUTPUT_PIX_FMT_RGB565LE
#define OUTPUT_PIX_FMT_RGB565LE		44
#endif
#ifndef OUTPUT_PIX_FMT_RGBA
#define OUTPUT_PIX_FMT_RGBA			28
#endif

//=======================================================
//图像处理
//=======================================================
typedef enum __VIDEO_FILTER_TYPE
{
	VIDEO_ROTATION_90_0				=		0,	//顺时针旋转90度
	VIDEO_ROTATION_90_1,						//逆时针旋转90度
	VIDEO_ROTATION_90_0_FLIP,					//顺时针旋转90度,再水平翻转
	VIDEO_ROTATION_90_1_FLIP,					//逆时针旋转90度,再垂直翻转

	VIDEO_TEXT,

}VIDEO_FILTER_TYPE;

//目前支持的音频采样位格式
typedef enum _FFCSample_Format {
	FFC_SAMPLE_FMT_NONE = -1,
	FFC_SAMPLE_FMT_U8,          ///< unsigned 8 bits
	FFC_SAMPLE_FMT_S16,         ///< signed 16 bits
	FFC_SAMPLE_FMT_S32,         ///< signed 32 bits
	FFC_SAMPLE_FMT_FLT,         ///< float
	FFC_SAMPLE_FMT_DBL,         ///< double
	FFC_SAMPLE_FMT_U8P,         ///< unsigned 8 bits, planar
	FFC_SAMPLE_FMT_S16P,        ///< signed 16 bits, planar
	FFC_SAMPLE_FMT_S32P,        ///< signed 32 bits, planar
	FFC_SAMPLE_FMT_FLTP,        ///< float, planar
	FFC_SAMPLE_FMT_DBLP,        ///< double, planar
	FFC_SAMPLE_FMT_S64,         ///< signed 64 bits
	FFC_SAMPLE_FMT_S64P,        ///< signed 64 bits, planar

	FFC_SAMPLE_FMT_NB           ///< Number of sample formats. DO NOT USE if linking dynamically
}FFCSample_Format;


//目前支持的色彩格式
typedef enum _FFCPixel_Format {
	FFC_PIX_FMT_YUV420P  =0,			//AV_PIX_FMT_YUV420P
	FFC_PIX_FMT_YUY2 = 1,			//PIX_FMT_YUYV422
	FFC_PIX_FMT_RGB24 = 3,			//AV_PIX_FMT_RGB24
	FFC_PIX_FMT_UYVY = 15,			//AV_PIX_FMT_UYVY422
	FFC_PIX_FMT_A8R8G8B8 = 28,			//PIX_FMT_BGRA
	FFC_PIX_FMT_X8R8G8B8 = 28,			//PIX_FMT_BGRA
	FFC_PIX_FMT_RGB565 = 37,			//PIX_FMT_RGB565LE
	FFC_PIX_FMT_RGB555 = 39,			//PIX_FMT_RGB555LE

}FFCPixel_Format;

typedef void *FFC_HANDLE;

extern "C"
{
	//编码函数声明===================================Start
	int	FFCODEC_API	FFE_Init(FFC_HANDLE *_handle);
	int	FFCODEC_API	FFE_Deinit(FFC_HANDLE *_handle);

	int FFCODEC_API	FFE_SetVideoEncodeParam(FFC_HANDLE _handle, int _videoCodec, int _width, int _height,
		int _fps, int _gop, int _bitrate, int _inputFormat/*0:YV12  3:RGB24*/, int threadNum);
	int FFCODEC_API	FFE_SetAudioEncodeParam(FFC_HANDLE _handle, int audioCodec, int bitRate, int sampleRate, int bitPerSample, int channels);

	int FFCODEC_API	FFE_EncodeVideo(FFC_HANDLE _handle, unsigned char *rawbuf, unsigned char *encbuf, int *encsize, bool* bKeyframe, int pts, int rgbImageFlip = 0x00);
	int	FFCODEC_API	FFE_EncodeAudio(FFC_HANDLE _handle, unsigned char *rawbuf, int rawsize, unsigned char *encbuf, int *encsize);

	//AAC编码
	int	FFCODEC_API	AAC_Init(FFC_HANDLE *_handle, int samplesPerSec, int sampleSize, int channels);
	int	FFCODEC_API	AAC_Encode(FFC_HANDLE _handle, int *pcmData, int pcmDataSize, unsigned char **_outputAACData, int *_aacDataSize);
	int	FFCODEC_API	AAC_Deinit(FFC_HANDLE *_handle);
	//编码函数声明===================================End

	//解码函数声明===================================Start
	int	FFCODEC_API	FFD_Init(FFC_HANDLE *_handle);
	int	FFCODEC_API	FFD_Deinit(FFC_HANDLE *_handle);

	int FFCODEC_API	FFD_SetVideoDecoderParam(FFC_HANDLE _handle, int _width, int _height, int _decoder, int _outformat, int multiThread = 1);
	int FFCODEC_API	FFD_SetAudioDecoderParam(FFC_HANDLE _handle, unsigned char _channel, unsigned int _sample_rate, 
		unsigned int bitsPerSample, unsigned int _decoder);

	int	FFCODEC_API	FFD_GetVideoDecoderInfo(FFC_HANDLE _handle, int *_decoder, int *_width, int *_height);

	//function name:	FFD_Decode
	//parameters:
		//Input:
	int	FFCODEC_API	FFD_DecodeVideo(FFC_HANDLE _handle, char *pInBuf, int inputSize, char **pOutBuf, int dstW, int dstH);

	//function name:	FFD_Decode2Buf
	//desc:				解码后的数据，直接送到指定的内存中
	int	FFCODEC_API	FFD_DecodeVideo2Buf(FFC_HANDLE _handle, char *_inbuf, int _bufsize, void *_outbuf[8], int _pitch);

	//你用这个函数解码
	int	FFCODEC_API	FFD_DecodeVideo3(FFC_HANDLE _handle, char *_inbuf, int _bufsize, void *yuvbuf, int dstW, int dstH);
	int	FFCODEC_API	FFD_ConvertDecodeFrameFormat(FFC_HANDLE _handle, char *dstData, int dstFormat, int dstW, int dstH);		//配合FFD_DecodeVideo3使用,将刚解码完的帧转换成指定格式
	int	FFCODEC_API	FFD_ConvertDecodeFrameFormat2(FFC_HANDLE _handle, char *dstData, int dstFormat, char *srcData, int srcFormat, int dstW, int dstH);

	int FFCODEC_API	FFD_DecodeVideoPacket(FFC_HANDLE _handle, char *pCodecCtx, unsigned char *avPacket, char **_outbuf);

	int	FFCODEC_API	FFD_DecodeAudio(FFC_HANDLE _handle, char *pInBuf, int inputSize, char *pOutBuf, int *outSize);
	int	FFCODEC_API	FFD_DecodeAudioPacket(FFC_HANDLE _handle, char *pCodecCtx, unsigned char *avPacket, char *pOutBuf, int *outSize);
	//解码函数声明===================================End

	//抓图（后缀为jpg或者png）
	int FFCODEC_API FFD_TakeSnap(char* file, int width, int height, unsigned char* buffer, FFCPixel_Format Format);

	//音频重采样（过滤）
	//音频重采样初始化
	int FFCODEC_API FFC_AudioResamplerInit(FFC_HANDLE _handle, int inSampleRate, FFCSample_Format inBitPerSample, int inChannels, int outSampleRate, int outBitPerSample, int outChannels);
	//重采样计算
	int FFCODEC_API FFC_AddAudioResamplerFrame(FFC_HANDLE _handle, char* frameBuffer, int frameSize, int nb_samples, int index, char* outFrameBuffer);

};


#endif

/*
//==================================================================
//==================================================================
//音频编码
//当前仅支持AAC编码
//==================================================================
//==================================================================

int channel = 2;
int sampleFrequency = 44100;
int bitrate = 150000;

FFE_HANDLE	ffeAudioHandle = NULL;
FFE_Init(&ffeAudioHandle);
FFE_SetAudioEncodeParam(ffeAudioHandle, ENCODER_AAC, channel, sampleFrequency, bitrate);

int enc_size = 0;
int ret = FFE_EncodeAudio(ffeAudioHandle, (unsigned short*)raw_buf, 4096, (unsigned char*)enc_buf, &enc_size);
if (ret == 0x00 && enc_size>0)
{
if (NULL != fOut)	fwrite(enc_buf, 1, enc_size, fOut);
}

while (1)
{
enc_size = 0;
ret = FFE_EncodeAudio(ffeAudioHandle, NULL, 0, (unsigned char*)enc_buf, &enc_size);
if (ret == 0x00 && enc_size>0)
{
if (NULL != fOut)	fwrite(enc_buf, 1, enc_size, fOut);
}
else	break;
}

FFE_Deinit(&ffeAudioHandle);



//==================================================================
//==================================================================
//视频编码
//当前仅支持H264编码
//==================================================================
//==================================================================
int	width = 1920;
int height = 1080;
int fps = 25;
int gop = 30;
int bitrate = 15000000;
int	intputformat = 3;		//3:rgb24  0:yv12

FFE_HANDLE	ffeVideoHandle = NULL;		//声明
FFE_Init(&ffeVideoHandle);	//初始化
FFE_SetVideoEncodeParam(ffeVideoHandle, ENCODER_H264, width, height, fps, gop, bitrate, intputformat);		//设置编码参数

char *enc_buf = new char[1920*1080];	//申请编码的内存空间
int enc_size = 0;
int ret = FFE_EncodeVideo(ffeVideoHandle, (unsigned char*)raw_buf, (unsigned char*)enc_buf, &enc_size, i+1);
if (ret == 0x00 && enc_size>0)
{
if (NULL != fOut)	fwrite(enc_buf, 1, enc_size, fOut);
}

//编码结束,释放该句柄
FFE_Deinit(&ffeVideoHandle);

delete []enc_buf;
*/
