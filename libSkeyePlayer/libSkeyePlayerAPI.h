/*
	Copyright (c) 2018-2023 OpenSKEYE.CN.  All rights reserved.
	Github: https://gitee.com/visual-opening/skeyeplayer
	WEChat: OpenSKEYE
	Website: http://www.OpenSKEYE.CN
	Author: Sword@OpenSKEYE.CN
*/

#ifndef __LIB_Skeye_PLAYER_API_H__
#define __LIB_Skeye_PLAYER_API_H__

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mmsystem.h>
#include "SkeyeRTSPClient\include\SkeyeRTSPClientAPI.h"
#include ".\D3DRender\include\x64\D3DRenderAPI.h"

#define LIB_SkeyePLAYER_API __declspec(dllexport)

typedef enum __RENDER_FORMAT
{
	DISPLAY_FORMAT_UNKNOWN  =	-1,
	DISPLAY_FORMAT_I420		=	0,
	DISPLAY_FORMAT_YV12		=	842094169,
	DISPLAY_FORMAT_YUY2		=	844715353,
	DISPLAY_FORMAT_UYVY		=	1498831189,
	DISPLAY_FORMAT_A8R8G8B8	=	21,
	DISPLAY_FORMAT_X8R8G8B8	=	22,
	DISPLAY_FORMAT_RGB565	=	23,
	DISPLAY_FORMAT_RGB555	=	25,
	DISPLAY_FORMAT_RGB24_GDI=	26,

	// D3D Render not support NV12 AND NV21 now yet! [12/6/2016 SwordTwelve]
	DISPLAY_FORMAT_NV12		=	MAKEFOURCC('N','V','1','2'),	
	DISPLAY_FORMAT_NV21		=	MAKEFOURCC('N','V','2','1'),	

}RENDER_FORMAT;

typedef struct tagSKEYE_PALYER_OSD
{
	char	stOSD[1024];
	DWORD	alpha;		//0-255
	DWORD	color;		//RGB(0xf9,0xf9,0xf9)
	DWORD	shadowcolor;		//RGB(0x4d,0x4d,0x4d) 全为0背景透明
	RECT	rect;		//OSD基于图像右上角显示区域
	int			size;		//just D3D Support
}SKEYE_PALYER_OSD;


typedef int (CALLBACK *MediaSourceCallBack)( int _channelId, int *_channelPtr, int _frameType, int _dataType, char *pBuf, int nBufLen, SKEYE_FRAME_INFO* _frameInfo);

LIB_SkeyePLAYER_API int	SkeyePlayer_Init(char* key);
LIB_SkeyePLAYER_API void SkeyePlayer_Release();

//decodeType硬件解码标识: 0=软解码 1=NvDecoder 2=IntelDecoder
LIB_SkeyePLAYER_API int SkeyePlayer_OpenStream(const char *url, HWND hWnd, RENDER_FORMAT renderFormat, int rtpovertcp, const char *username, const char *password, MediaSourceCallBack callback=NULL, void *userPtr=NULL, char* startTime=NULL, char* endTime=NULL, float fScale = 1.0f, int decodeType=0, bool bInnerShow=true);
LIB_SkeyePLAYER_API void SkeyePlayer_CloseStream(int channelId);

//////////////////////////////////////////////////////////////////////////////////////
// RTSP回放调用接口
/*  设置播放速度 */
LIB_SkeyePLAYER_API int SkeyePlayer_SetStreamSpeed(int channelId, float fSpeed);
/* 跳转播放时间 */
LIB_SkeyePLAYER_API int SkeyePlayer_SeekStream(int channelId, const char *playTime);
/* 暂停 */
LIB_SkeyePLAYER_API int SkeyePlayer_PauseStream(int channelId);
/* 恢复播放 */
LIB_SkeyePLAYER_API int SkeyePlayer_ResumeStream(int channelId);
///////////////////////////////////////////////////////////////////////////////////////

LIB_SkeyePLAYER_API int SkeyePlayer_SetFrameCache(int channelId, int cache);
LIB_SkeyePLAYER_API int SkeyePlayer_SetShownToScale(int channelId, int shownToScale);
LIB_SkeyePLAYER_API int SkeyePlayer_SetDecodeType(int channelId, int decodeKeyframeOnly);
LIB_SkeyePLAYER_API int SkeyePlayer_SetRenderRect(int channelId, LPRECT lpSrcRect);
LIB_SkeyePLAYER_API int SkeyePlayer_ShowStatisticalInfo(int channelId, int show);
LIB_SkeyePLAYER_API int SkeyePlayer_ShowOSD(int channelId, int show, SKEYE_PALYER_OSD osd);

LIB_SkeyePLAYER_API int SkeyePlayer_SetDragStartPoint(int channelId, POINT pt);
LIB_SkeyePLAYER_API int SkeyePlayer_SetDragEndPoint(int channelId, POINT pt);
LIB_SkeyePLAYER_API int SkeyePlayer_ResetDragPoint(int channelId);

LIB_SkeyePLAYER_API int SkeyePlayer_StartManuRecording(int channelId);
LIB_SkeyePLAYER_API int SkeyePlayer_StopManuRecording(int channelId);

LIB_SkeyePLAYER_API int SkeyePlayer_PlaySound(int channelId);
LIB_SkeyePLAYER_API int SkeyePlayer_StopSound();

LIB_SkeyePLAYER_API int	SkeyePlayer_SetManuRecordPath(int channelId, const char* recordPath);
LIB_SkeyePLAYER_API int	SkeyePlayer_SetManuPicShotPath(int channelId, const char* shotPath);

LIB_SkeyePLAYER_API int	SkeyePlayer_StartManuPicShot(int channelId);//pThread->manuScreenshot
LIB_SkeyePLAYER_API int	SkeyePlayer_StopManuPicShot(int channelId);

//视频窗口特效叠加相关接口函数
LIB_SkeyePLAYER_API int		SkeyePlayer_AddBuoy(int channelId, D3D9_BUOY buoy);
//LIB_SkeyePLAYER_API int		SkeyePlayer_AddSign(int channelId);
//LIB_SkeyePLAYER_API int		SkeyePlayer_AddLine(int channelId);
//LIB_SkeyePLAYER_API int		SkeyePlayer_AddRect(int channelId);
//LIB_SkeyePLAYER_API int		SkeyePlayer_AddZone(int channelId);
//LIB_SkeyePLAYER_API int		SkeyePlayer_AddCircular(int channelId);


//视频窗口鼠标消息处理
LIB_SkeyePLAYER_API int SkeyePlayer_DoMouseEvent(int channelId, Render_MouseEventType eType, const RECT rcWnd, char* sGetValue);

#endif
