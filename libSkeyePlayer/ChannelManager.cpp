/*
	Copyright (c) 2018-2023 OpenSKEYE.CN.  All rights reserved.
	Github: https://gitee.com/visual-opening/skeyeplayer
	WEChat: OpenSKEYE
	Website: http://www.OpenSKEYE.CN
	Author: Sword@OpenSKEYE.CN
*/
#include "ChannelManager.h"
#include <time.h>
#include "vstime.h"
#include "trace.h"

//动态加载NvDecoder dll函数指针
funcNvDecoder_Create  pFuncNvDecoder_Create = NULL;
funcNvDecoder_Decode  pFuncNvDecoder_Decode = NULL;
funcNvDecoder_Release pFuncNvDecoder_Release = NULL;
funcNvDecoder_DecodeHW  pFuncNvDecoder_DecodeHW = NULL;


#define		CHANNEL_ID_GAIN			1000

#define	__DELETE_ARRAY(x)	{if (NULL!=x) {delete []x;x=NULL;}}

int CALLBACK __RTSPSourceCallBack( int _channelId, void *_channelPtr, int _frameType, char *pBuf, SKEYE_FRAME_INFO* _frameInfo);
int _SKEYELIVESource_Callback(SkeyeLivePlayer::CallbackType type, uint64_t data, uint32_t dataSize, FrameInfo *frameInfo);

bool  CheckHasNvidiaCard()
{

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
	HMODULE hDll = LoadLibrary(L"nvcuda.dll");
	if (hDll)
	{
		FreeLibrary(hDll);
		return true;
	}

#endif
	return false;
}

CChannelManager	*pChannelManager = NULL;
CChannelManager::CChannelManager(void)
{
	_hDLL = NULL;
	if (CheckHasNvidiaCard())
	{
		_hDLL = LoadLibrary(TEXT("NvDecoder.dll"));
		if (_hDLL)
		{
			pFuncNvDecoder_Create = (funcNvDecoder_Create)GetProcAddress(_hDLL, ("NvDecoder_Create"));
			pFuncNvDecoder_Decode = (funcNvDecoder_Decode)GetProcAddress(_hDLL, ("NvDecoder_Decode"));
			pFuncNvDecoder_DecodeHW = (funcNvDecoder_DecodeHW)GetProcAddress(_hDLL, ("NvDecoder_DecodeHW"));
			pFuncNvDecoder_Release = (funcNvDecoder_Release)GetProcAddress(_hDLL, ("NvDecoder_Release"));
		}
	}

	pRealtimePlayThread	=	NULL;
	pAudioPlayThread	=	NULL;
	memset(&d3dAdapter, 0x00, sizeof(D3D_ADAPTER_T));

	InitializeCriticalSection(&crit);
}

CChannelManager::~CChannelManager(void)
{
	Release();
	DeleteCriticalSection(&crit);
}

int	CChannelManager::Initial()
{
	if (NULL == pRealtimePlayThread)
	{
		pRealtimePlayThread = new PLAY_THREAD_OBJ[MAX_CHANNEL_NUM];
		if (NULL == pRealtimePlayThread)		return -1;

		memset(&pRealtimePlayThread[0], 0x00, sizeof(PLAY_THREAD_OBJ)*MAX_CHANNEL_NUM);

		for (int i=0; i<MAX_CHANNEL_NUM; i++)
		{
			InitializeCriticalSection(&pRealtimePlayThread[i].crit);
			pRealtimePlayThread[i].renderFormat = D3D_FORMAT_YV12;		//默认为GDI显示
			pRealtimePlayThread[i].channelId = i+1;
		}
	}

	if (NULL == pChannelManager)		pChannelManager	=	this;

	return 0;
}

void CChannelManager::Release()
{
	if (NULL != pRealtimePlayThread)
	{
		for (int i=0; i<MAX_CHANNEL_NUM; i++)
		{
			if (NULL != pRealtimePlayThread[i].nvsHandle)
			{
				if (pRealtimePlayThread[i].nLiveType == 1)
				{
					SkeyeLivePlayer* pSkeyeLiveClient = (SkeyeLivePlayer*)pRealtimePlayThread[i].nvsHandle;
					pSkeyeLiveClient->stopLiveConnection();
					delete pRealtimePlayThread[i].nvsHandle;
					pRealtimePlayThread[i].nvsHandle = NULL;
				}
				else {
					SkeyeRTSP_CloseStream(pRealtimePlayThread[i].nvsHandle);
					SkeyeRTSP_Deinit(&pRealtimePlayThread[i].nvsHandle);
				}
			}

			ClosePlayThread(&pRealtimePlayThread[i]);
			DeleteCriticalSection(&pRealtimePlayThread[i].crit);
			for (int idx=0; idx<MAX_DECODER_NUM; idx++)
			{
				if (pRealtimePlayThread[i].decoderObj[idx].pIntelDecoder)
				{
					Release_IntelHardDecoder(pRealtimePlayThread[i].decoderObj[idx].pIntelDecoder);
				}
				if (NULL != pRealtimePlayThread[i].decoderObj[idx].pNvDecoder)
				{
					if (pFuncNvDecoder_Release)
						pFuncNvDecoder_Release(pRealtimePlayThread[i].decoderObj[idx].pNvDecoder);
					pRealtimePlayThread[i].decoderObj[idx].pNvDecoder = NULL;
					//int ret = NvDecoder_Uninitsize();
				}
			}

		}
		__DELETE_ARRAY(pRealtimePlayThread);
	}
	//销毁音频播放线程
	if (NULL != pAudioPlayThread)
	{
		if (NULL != pAudioPlayThread->pSoundPlayer)
		{
			pAudioPlayThread->pSoundPlayer->Close();
			delete pAudioPlayThread->pSoundPlayer;
			pAudioPlayThread->pSoundPlayer = NULL;
		}
		delete pAudioPlayThread;
		pAudioPlayThread = NULL;
	}
}

//struct FrameInfo
//{
//	enum class VideoCodec : uint8_t {
//		Encode_H264,
//		Encode_H265
//	};
//
//	enum class AudioCodec : uint8_t {
//		Encode_PCM,
//		Encode_AAC
//	};
//
//	union {
//		struct VideoInfo {
//			VideoCodec codec;
//			int width;
//			int height;
//			int fps;
//		} _video;
//		struct AudioInfo {
//			AudioCodec codec;
//			int channels;
//			int sampleSize;
//			int sampleRate;
//		} _audio;
//		struct MouseInfo {
//			int qt_button;
//		} _mouse;
//		struct KeyboardInfo {
//			int qt_keyboardModifier;
//			int qt_key;
//		} _keyboard;
//	} _info;
//
//	void clear() { memset(&_info, 0, sizeof(_info)); }
//};

int _SKEYELIVESource_Callback(SkeyeLivePlayer::CallbackType type, uint64_t data, uint32_t dataSize, FrameInfo *frameInfo, void *userPtr)
{
	printf("dataSize=%d\n", dataSize);
	PLAY_THREAD_OBJ	*pPlayThread = (PLAY_THREAD_OBJ *)userPtr;
	if (NULL == pPlayThread)			
		return -1;
	if (NULL == pChannelManager)	
		return -1;
	//VideoFrame,
	//AudioInputFrame,
	//AudioOutputFrame,
	//MouseEvent,
	//KeyboardEvent
	//#define SKEYE_SDK_VIDEO_FRAME_FLAG	0x00000001		/* 视频帧标志 */
	//#define SKEYE_SDK_AUDIO_FRAME_FLAG	0x00000002		/* 音频帧标志 */
	int _frameType = 0;
	SKEYE_FRAME_INFO _frameInfo;
	memset(&_frameInfo, 0x00, sizeof(SKEYE_FRAME_INFO));
	_frameInfo.length = dataSize;
	if (SkeyeLivePlayer::CallbackType::VideoFrame == type)
	{
		_frameType = SKEYE_SDK_VIDEO_FRAME_FLAG;

		//SKEYE_SDK_VIDEO_CODEC_H265
		_frameInfo.codec = (frameInfo->_info._video.codec == FrameInfo::VideoCodec::Encode_H265) ? SKEYE_SDK_VIDEO_CODEC_H265 : SKEYE_SDK_VIDEO_CODEC_H264;
		_frameInfo.fps = frameInfo->_info._video.fps;
		_frameInfo.width = frameInfo->_info._video.width;
		_frameInfo.height = frameInfo->_info._video.height;

	}
	else if (SkeyeLivePlayer::CallbackType::AudioInputFrame == type)
	{
		_frameType = SKEYE_SDK_AUDIO_FRAME_FLAG;
		//enum class AudioCodec : uint8_t {
		//	Encode_PCM,
		//	Encode_AAC
		_frameInfo.codec = (frameInfo->_info._audio.codec == FrameInfo::AudioCodec::Encode_AAC) ? SKEYE_SDK_AUDIO_CODEC_AAC : SKEYE_SDK_AUDIO_CODEC_PCM;
		_frameInfo.bits_per_sample = frameInfo->_info._audio.sampleSize;
		_frameInfo.channels = frameInfo->_info._audio.channels;
		_frameInfo.sample_rate = frameInfo->_info._audio.sampleRate;

	}
	else if (SkeyeLivePlayer::CallbackType::AudioOutputFrame == type)
	{
		//输出音频暂不作处理
		return 0;
		_frameType = SKEYE_SDK_AUDIO_FRAME_FLAG;
		_frameInfo.codec = (frameInfo->_info._audio.codec == FrameInfo::AudioCodec::Encode_AAC) ? SKEYE_SDK_AUDIO_CODEC_AAC : SKEYE_SDK_AUDIO_CODEC_PCM;
		_frameInfo.bits_per_sample = frameInfo->_info._audio.sampleSize;
		_frameInfo.channels = frameInfo->_info._audio.channels;
		_frameInfo.sample_rate = frameInfo->_info._audio.sampleRate;
	}

	pChannelManager->ProcessData(pPlayThread->channelId-1, _frameType, (char*)data, &_frameInfo);

	return 0;
}


int	CChannelManager::OpenStream(const char *url, HWND hWnd, RENDER_FORMAT renderFormat, int _rtpovertcp, 
	const char *username, const char *password, MediaSourceCallBack callback, void *userPtr, 
	char* startTime, char* endTime, float fScale, int decodeType, bool bInnerShow)
{
	if (NULL == pRealtimePlayThread)			return -1;
	if ( (NULL == url) || (0==strcmp(url, "\0")))		return -1;

	int iNvsIdx = -1;
	EnterCriticalSection(&crit);
	do
	{
		for (int i=0; i<MAX_CHANNEL_NUM; i++)
		{
			if (NULL == pRealtimePlayThread[i].nvsHandle)
			{
				iNvsIdx = i;
				break;
			}
		}

		if (iNvsIdx == -1)	
			break;

		if (0 == strncmp(url, "skeyelive", 9) || 0 == strncmp(url, "SKEYELIVE", 9))
			pRealtimePlayThread[iNvsIdx].nLiveType = 1;
		else
			pRealtimePlayThread[iNvsIdx].nLiveType = 0;

		if (pRealtimePlayThread[iNvsIdx].nLiveType == 1)
		{
			SkeyeLivePlayer* pSkeyeLiveClient = new SkeyeLivePlayer(_rtpovertcp == 0x01 ? 
				SkeyeLivePlayer::ConnectionType::Type_Tcp:SkeyeLivePlayer::ConnectionType::Type_Kcp,
				(SkeyeLivePlayer::DecoderType)decodeType);//Type_Software
			 pRealtimePlayThread[iNvsIdx].nvsHandle = pSkeyeLiveClient;
			 pSkeyeLiveClient->startLiveConnection(url, SkeyeLivePlayer::VideoPixelFormat::Format_RGBA32, 
				 _SKEYELIVESource_Callback, &pRealtimePlayThread[iNvsIdx], false);
		}
		else
		{
			SkeyeRTSP_Init(&pRealtimePlayThread[iNvsIdx].nvsHandle);
			if (NULL == pRealtimePlayThread[iNvsIdx].nvsHandle)		
				break;	//退出while循环

			unsigned int mediaType = MEDIA_TYPE_VIDEO | MEDIA_TYPE_AUDIO | SKEYE_SDK_EVENT_FRAME_FLAG;
			SkeyeRTSP_SetCallback(pRealtimePlayThread[iNvsIdx].nvsHandle, __RTSPSourceCallBack);
			SkeyeRTSP_OpenStream(pRealtimePlayThread[iNvsIdx].nvsHandle, iNvsIdx, (char*)url,
				_rtpovertcp == 0x01 ? SKEYE_RTP_OVER_TCP : SKEYE_RTP_OVER_UDP, mediaType,
				(char*)username, (char*)password, (int*)&pRealtimePlayThread[iNvsIdx], 1000, 0, 1, 0, startTime, endTime, fScale);
		}
		for (int nI=0; nI<MAX_DECODER_NUM; nI++)
		{
			pRealtimePlayThread[iNvsIdx].decoderObj[nI].nHardDecode = decodeType;
			pRealtimePlayThread[iNvsIdx].decoderObj[nI].bInnerShow = bInnerShow;	
		}
		memcpy(pRealtimePlayThread[iNvsIdx].url, url, strlen(url) + 1);
		pRealtimePlayThread[iNvsIdx].pCallback = callback;
		pRealtimePlayThread[iNvsIdx].pUserPtr = userPtr;

		pRealtimePlayThread[iNvsIdx].hWnd = hWnd;
		pRealtimePlayThread[iNvsIdx].renderFormat = (D3D_SUPPORT_FORMAT)renderFormat;
		CreatePlayThread(&pRealtimePlayThread[iNvsIdx]);

	}while (0);
	LeaveCriticalSection(&crit);

	if (iNvsIdx >= 0)	iNvsIdx += CHANNEL_ID_GAIN;
	return iNvsIdx;
}

void CChannelManager::CloseStream(int channelId)
{
	if (NULL == pRealtimePlayThread)			return;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return;

	EnterCriticalSection(&crit);
	if (pRealtimePlayThread[iNvsIdx].nLiveType == 1)
	{
		SkeyeLivePlayer* pSkeyeLiveClient = (SkeyeLivePlayer*)pRealtimePlayThread[iNvsIdx].nvsHandle;
		pSkeyeLiveClient->stopLiveConnection();
		delete pRealtimePlayThread[iNvsIdx].nvsHandle;
		pRealtimePlayThread[iNvsIdx].nvsHandle = NULL;
	}
	else {
		//关闭rtsp client
		SkeyeRTSP_CloseStream(pRealtimePlayThread[iNvsIdx].nvsHandle);
		SkeyeRTSP_Deinit(&pRealtimePlayThread[iNvsIdx].nvsHandle);
	}
	//关闭播放线程
	ClosePlayThread(&pRealtimePlayThread[iNvsIdx]);
	pRealtimePlayThread[iNvsIdx].nvsHandle = NULL;
	pRealtimePlayThread[iNvsIdx].nLiveType = 0;
	LeaveCriticalSection(&crit);
}

/*  设置播放速度（播放直播流时无效） */
int CChannelManager::SetStreamSpeed(int channelId, float fSpeed)
{
	if (NULL == pRealtimePlayThread)			
		return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)	
		return -2;
	int ret = 0;
	if (pRealtimePlayThread[iNvsIdx].nLiveType == 0)
	{
		EnterCriticalSection(&crit);
		//设置rtsp回放播放速度
		ret = SkeyeRTSP_SetStreamSpeed(pRealtimePlayThread[iNvsIdx].nvsHandle, fSpeed);
		LeaveCriticalSection(&crit);
	}
	return ret;
}
/* 跳转播放时间 */
int CChannelManager::SeekStream(int channelId, const char *playTime)
{
	if (NULL == pRealtimePlayThread)
		return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)
		return -2;
	int ret = 0;
	if (pRealtimePlayThread[iNvsIdx].nLiveType == 0)
	{
		EnterCriticalSection(&crit);
		//设置rtsp回放播放时间点，定位播放位置
		ret = SkeyeRTSP_SeekStream(pRealtimePlayThread[iNvsIdx].nvsHandle, playTime);
		LeaveCriticalSection(&crit);
	}
	return ret;
}
/* 暂停 */
int CChannelManager::PauseStream(int channelId)
{
	if (NULL == pRealtimePlayThread)
		return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)
		return -2;
	int ret = 0;
	if (pRealtimePlayThread[iNvsIdx].nLiveType == 0)
	{
		EnterCriticalSection(&crit);

		ret = SkeyeRTSP_PauseStream(pRealtimePlayThread[iNvsIdx].nvsHandle);
		LeaveCriticalSection(&crit);
	}
	return ret;

}
/* 恢复播放 */
int CChannelManager::ResumeStream(int channelId)
{
	if (NULL == pRealtimePlayThread)
		return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)
		return -2;
	int ret = 0;
	if (pRealtimePlayThread[iNvsIdx].nLiveType == 0)
	{
		EnterCriticalSection(&crit);

		ret = SkeyeRTSP_ResumeStream(pRealtimePlayThread[iNvsIdx].nvsHandle);
		LeaveCriticalSection(&crit);
	}
	return ret;
}


int	CChannelManager::ShowStatisticalInfo(int channelId, int _show)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].showStatisticalInfo = _show;
	return 0;
}
int	CChannelManager::ShowOSD(int channelId, int _show, SKEYE_PALYER_OSD osd)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].showOSD = _show;
	//memcpy(&pRealtimePlayThread[iNvsIdx].osd,  &osd, sizeof(SKEYE_PALYER_OSD));
	if (_show)
	{
		pRealtimePlayThread[iNvsIdx].osd.alpha = osd.alpha;
		if (pRealtimePlayThread[iNvsIdx].osd.size != osd.size)
		{
			pRealtimePlayThread[iNvsIdx].resetD3d = true;
		}
		pRealtimePlayThread[iNvsIdx].osd.size = osd.size;
		pRealtimePlayThread[iNvsIdx].osd.color = osd.color;
		pRealtimePlayThread[iNvsIdx].osd.rect.left = osd.rect.left;
		pRealtimePlayThread[iNvsIdx].osd.rect.right = osd.rect.right;
		pRealtimePlayThread[iNvsIdx].osd.rect.top = osd.rect.top;
		pRealtimePlayThread[iNvsIdx].osd.rect.bottom = osd.rect.bottom;
		pRealtimePlayThread[iNvsIdx].osd.shadowcolor = osd.shadowcolor;

		strcpy(pRealtimePlayThread[iNvsIdx].osd.stOSD, osd.stOSD);
	}

	return 0;
}

int	CChannelManager::SetFrameCache(int channelId, int _cache)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].frameCache = _cache;
	return 0;
}
int	CChannelManager::SetShownToScale(int channelId, int ShownToScale)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].ShownToScale = ShownToScale;
	return 0;
}
int	CChannelManager::SetDecodeType(int channelId, int _decodeKeyframeOnly)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].decodeKeyFrameOnly = _decodeKeyframeOnly;
	return 0;
}
int	CChannelManager::SetRenderRect(int channelId, LPRECT lpSrcRect)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (NULL == lpSrcRect)	SetRectEmpty(&pRealtimePlayThread[iNvsIdx].rcSrcRender);
	else					CopyRect(&pRealtimePlayThread[iNvsIdx].rcSrcRender, lpSrcRect);

	return 0;
}
int	CChannelManager::DrawLine(int channelId, LPRECT lpRect)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (NULL == lpRect)		memset(&pRealtimePlayThread[iNvsIdx].d3d9Line, 0x00, sizeof(D3D9_LINE));
	else
	{
		memset(&pRealtimePlayThread[iNvsIdx].d3d9Line, 0x00, sizeof(D3D9_LINE));
		pRealtimePlayThread[iNvsIdx].d3d9Line.dwColor = RGB(0x0F, 0xF0, 0x00);
		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[0].x = lpRect->left;
		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[0].y  = lpRect->top;

		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[1].x = lpRect->right;
		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[1].y = lpRect->top;

		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[2].x = lpRect->right;
		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[2].y = lpRect->bottom;

		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[3].x = lpRect->left;
		pRealtimePlayThread[iNvsIdx].d3d9Line.pNodes[3].y = lpRect->bottom;
		pRealtimePlayThread[iNvsIdx].d3d9Line.uiTotalNodes = 4;
	}

	return 0;
}

int		CChannelManager::SetDragStartPoint(int channelId, POINT pt)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (pRealtimePlayThread[iNvsIdx].renderFormat == GDI_FORMAT_RGB24)
	{
#ifdef _WIN64
		GDI_SetDragStartPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt.x, pt.y, 1);
#else
		RGB_SetDragStartPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt);
#endif
	}
	else
	{
#ifdef _WIN64
		D3D_SetStartPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt.x, pt.y);
#else
		D3D_SetStartPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt);
#endif
	}
	return 0;
}
int		CChannelManager::SetDragEndPoint(int channelId, POINT pt)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (pRealtimePlayThread[iNvsIdx].renderFormat == GDI_FORMAT_RGB24)
	{
#ifdef _WIN64
		GDI_SetDragEndPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt.x, pt.y);
#else
		RGB_SetDragEndPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt);
#endif
	}
	else
	{
#ifdef _WIN64
		D3D_SetEndPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt.x, pt.y);
#else
		D3D_SetEndPoint(pRealtimePlayThread[iNvsIdx].d3dHandle, pt);
#endif
	}
	return 0;
}
int		CChannelManager::ResetDragPoint(int channelId)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (pRealtimePlayThread[iNvsIdx].renderFormat == GDI_FORMAT_RGB24)
	{
#ifdef _WIN64
		GDI_ResetDragPoint(pRealtimePlayThread[iNvsIdx].d3dHandle);
#else
		RGB_ResetDragPoint(pRealtimePlayThread[iNvsIdx].d3dHandle);
#endif
	}
	else
	{
		D3D_ResetSelZone(pRealtimePlayThread[iNvsIdx].d3dHandle);
	}
	return 0;
}

int	CChannelManager::AddBuoy(int channelId, D3D9_BUOY buoy)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)	return -1;

	if (pRealtimePlayThread[iNvsIdx].renderFormat == GDI_FORMAT_RGB24)
	{

	}
	else
	{
		D3D_AddBuoy(pRealtimePlayThread[iNvsIdx].d3dHandle,&buoy);
	}
	return 1;
}

int  CChannelManager::DoMouseEvent(int channelId, Render_MouseEventType eType, RECT rcWnd, char* sRetValue)
{
	int ret = -1;
	if (NULL == pRealtimePlayThread)			
		return ret;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx >= MAX_CHANNEL_NUM)	return -2;

	if (pRealtimePlayThread[iNvsIdx].renderFormat == GDI_FORMAT_RGB24)
	{

	}
	else
	{
		ret = D3D_DoMouseEvent(pRealtimePlayThread[iNvsIdx].d3dHandle, eType, rcWnd, sRetValue);
	}
	return ret;
}

//播放声音		2023.12.01
int	CChannelManager::PlaySound(int channelId)
{
	int ret = -1;
	if (NULL == pAudioPlayThread)
	{
		pAudioPlayThread = new AUDIO_PLAY_THREAD_OBJ;
		if (NULL == pAudioPlayThread)		return ret;
		memset(pAudioPlayThread, 0x00, sizeof(AUDIO_PLAY_THREAD_OBJ));
	}

	ClearAllSoundData();		//如果当前正在播放其它音频,则清空


	int iNvsIdx = channelId - CHANNEL_ID_GAIN + 1;

	if (pAudioPlayThread->channelId != iNvsIdx)
	{
		pAudioPlayThread->channelId = iNvsIdx;//channelId;
	
		if (NULL != pAudioPlayThread->pSoundPlayer)
		{
			pAudioPlayThread->pSoundPlayer->Close();
		}

		pAudioPlayThread->audiochannels	=	0;
		pAudioPlayThread->samplerate	=	0;
		pAudioPlayThread->bitpersample	=	0;
		ret = 0;
	}

	return ret;
}
int	CChannelManager::StopSound()
{
	if (NULL == pAudioPlayThread)		return 0;

	if (NULL != pAudioPlayThread->pSoundPlayer)
	{
		pAudioPlayThread->pSoundPlayer->Close();
	}
	pAudioPlayThread->audiochannels  = 0;
	pAudioPlayThread->channelId = -1;
	return 0;
}
void CChannelManager::ClearAllSoundData()
{
	if (NULL != pChannelManager)
	{
		if (NULL != pChannelManager->pAudioPlayThread)
		{
			if (NULL != pChannelManager->pAudioPlayThread->pSoundPlayer)
			{
				pChannelManager->pAudioPlayThread->pSoundPlayer->Clear();
			}
		}
	}
}

void CChannelManager::CreatePlayThread(PLAY_THREAD_OBJ	*_pPlayThread)
{
	if (NULL == _pPlayThread)		return;

	if (_pPlayThread->decodeThread.flag == 0x00)
	{
		//_pPlayThread->ch_tally = 0;
		_pPlayThread->decodeThread.flag = 0x01;

		_pPlayThread->decodeThread.hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_lpDecodeThread, _pPlayThread, 0, NULL);
		while (_pPlayThread->decodeThread.flag!=0x02 && _pPlayThread->decodeThread.flag!=0x00)	{Sleep(100);}
		if (NULL != _pPlayThread->decodeThread.hThread)
		{
			SetThreadPriority(_pPlayThread->decodeThread.hThread, /*THREAD_PRIORITY_HIGHEST*/THREAD_PRIORITY_NORMAL);
		}
	}
	if (_pPlayThread->displayThread.flag == 0x00)
	{
		_pPlayThread->displayThread.flag = 0x01;
		_pPlayThread->displayThread.hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_lpDisplayThread, _pPlayThread, 0, NULL);
		while (_pPlayThread->displayThread.flag!=0x02 && _pPlayThread->displayThread.flag!=0x00)	{Sleep(100);}
		if (NULL != _pPlayThread->displayThread.hThread)
		{
			SetThreadPriority(_pPlayThread->displayThread.hThread, /*THREAD_PRIORITY_HIGHEST*/THREAD_PRIORITY_NORMAL);
		}
	}
}
void CChannelManager::ClosePlayThread(PLAY_THREAD_OBJ	*_pPlayThread)
{
	if (NULL == _pPlayThread)		return;

	//关闭解码线程
	if (_pPlayThread->decodeThread.flag != 0x00)
	{
#ifdef _DEBUG
		_TRACE("关闭解码线程[%d]\n", _pPlayThread->channelId);
#endif
		_pPlayThread->decodeThread.flag = 0x03;
		while (_pPlayThread->decodeThread.flag!=0x00)	{Sleep(100);}
	}
	if (NULL != _pPlayThread->decodeThread.hThread)
	{
		CloseHandle(_pPlayThread->decodeThread.hThread);
		_pPlayThread->decodeThread.hThread = NULL;
	}
	for (int i=0; i<MAX_DECODER_NUM; i++)		//关闭解码器
	{
		if (NULL != _pPlayThread->decoderObj[i].ffDecoder)
		{
			FFD_Deinit(&_pPlayThread->decoderObj[i].ffDecoder);
			_pPlayThread->decoderObj[i].ffDecoder = NULL;
		}
		if (NULL != _pPlayThread->decoderObj[i].pIntelDecoder)
		{
			Release_IntelHardDecoder(_pPlayThread->decoderObj[i].pIntelDecoder);
			_pPlayThread->decoderObj[i].pIntelDecoder = NULL;
		}
		if (NULL != _pPlayThread->decoderObj[i].pNvDecoder)
		{
			if (pFuncNvDecoder_Release)
				pFuncNvDecoder_Release(_pPlayThread->decoderObj[i].pNvDecoder);
			_pPlayThread->decoderObj[i].pNvDecoder = NULL;
			//int ret = NvDecoder_Uninitsize();
		}

		_pPlayThread->decoderObj[i].nHardDecode = 0;
		memset(&_pPlayThread->decoderObj[i].codec, 0x00, sizeof(CODEC_T));
		_pPlayThread->decoderObj[i].yuv_size = 0;
	}

	//关闭播放线程
	if (_pPlayThread->displayThread.flag != 0x00)
	{
#ifdef _DEBUG
		_TRACE("关闭播放线程[%d]\n", _pPlayThread->channelId);
#endif
		_pPlayThread->displayThread.flag = 0x03;
		while (_pPlayThread->displayThread.flag!=0x00)	{Sleep(100);}
	}
	if (NULL != _pPlayThread->displayThread.hThread)
	{
		CloseHandle(_pPlayThread->displayThread.hThread);
		_pPlayThread->displayThread.hThread = NULL;
	}
	for (int i=0; i<MAX_YUV_FRAME_NUM; i++)
	{
		__DELETE_ARRAY(_pPlayThread->yuvFrame[i].pYuvBuf);
		memset(&_pPlayThread->yuvFrame[i].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
		_pPlayThread->yuvFrame[i].Yuvsize = 0;
	}

	//释放队列
	if (NULL != _pPlayThread->pAVQueue)
	{
		SSQ_Deinit(_pPlayThread->pAVQueue);
		delete _pPlayThread->pAVQueue;
		_pPlayThread->pAVQueue = NULL;
	}
	_pPlayThread->initQueue = 0x00;
	_pPlayThread->frameCache	=	NULL;

	memset(&_pPlayThread->decodeThread, 0x00, sizeof(THREAD_OBJ));
	memset(&_pPlayThread->displayThread, 0x00, sizeof(THREAD_OBJ));
	_pPlayThread->hWnd	=	NULL;
}



int	CChannelManager::SetAudioParams(unsigned int _channel, unsigned int _samplerate, unsigned int _bitpersample)
{
	if (NULL == pAudioPlayThread)		return -1;

	WAVEFORMATEX wfx = {0,};
	wfx.cbSize = sizeof(WAVEFORMATEX);
	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nSamplesPerSec = _samplerate;
	wfx.wBitsPerSample = _bitpersample;
	wfx.nChannels = _channel;
	wfx.nBlockAlign =  wfx.nChannels * ( wfx.wBitsPerSample / 8 );//(wfx.wBitsPerSample*wfx.nChannels)>>3; //wfx.nChannels * ( wfx.wBitsPerSample / 8 );
	wfx.nAvgBytesPerSec = wfx.nBlockAlign * wfx.nSamplesPerSec;

	wfx.nAvgBytesPerSec	= wfx.nSamplesPerSec * wfx.nChannels * wfx.wBitsPerSample / 8;
	wfx.nBlockAlign		= wfx.nChannels * wfx.wBitsPerSample / 8;

	if (NULL == pAudioPlayThread->pSoundPlayer)
	{
		pAudioPlayThread->pSoundPlayer = new CSoundPlayer();
	}
	if (NULL != pAudioPlayThread->pSoundPlayer)
	{
		pAudioPlayThread->pSoundPlayer->Close();
		pAudioPlayThread->pSoundPlayer->Open(wfx);
	}
	pAudioPlayThread->audiochannels	=	_channel;
	return 0;
}

////目前支持的色彩格式
//typedef enum _FFCPixel_Format {
//	FFC_PIX_FMT_YUV420P = 0,			//AV_PIX_FMT_YUV420P
//	FFC_PIX_FMT_YUY2 = 1,			//PIX_FMT_YUYV422
//	FFC_PIX_FMT_RGB24 = 2,			//AV_PIX_FMT_RGB24
//	FFC_PIX_FMT_UYVY = 15,			//AV_PIX_FMT_UYVY422
//	FFC_PIX_FMT_A8R8G8B8 = 28,			//PIX_FMT_BGRA
//	FFC_PIX_FMT_X8R8G8B8 = 28,			//PIX_FMT_BGRA
//	FFC_PIX_FMT_RGB565 = 37,			//PIX_FMT_RGB565LE
//	FFC_PIX_FMT_RGB555 = 39,			//PIX_FMT_RGB555LE
//
//}FFCPixel_Format;

void ParseDecoder2Render(int *_decoder, int _width, int _height, int renderFormat, int *yuvsize)
{
	if (NULL == _decoder)		return;
	int nDecoder = 0x00;
	int nYUVSize = 0x00;
	if (renderFormat == D3D_FORMAT_UYVY)
	{
		nDecoder	= FFC_PIX_FMT_UYVY;	//PIX_FMT_UYVY422
		nYUVSize	=	_width*_height*2+1;
	}
	else if (renderFormat == D3D_FORMAT_YV12)
	{
		nDecoder	= FFC_PIX_FMT_YUV420P;//OUTPUT_PIX_FMT_YUV420P;
		nYUVSize	=	_width*_height*3/2+1;
	}
	else if (renderFormat == D3D_FORMAT_YUY2)
	{
		nDecoder	= FFC_PIX_FMT_YUY2;//OUTPUT_PIX_FMT_YUV422;
		nYUVSize	=	_width*_height*2+1;
	}
	else if (renderFormat == D3D_FORMAT_RGB565)				//23 ok
	{
		nDecoder	= FFC_PIX_FMT_RGB565;
		nYUVSize	=	_width*_height*2+1;
	}
	else if (renderFormat == D3D_FORMAT_RGB555)				//23 ok
	{
		nDecoder	= FFC_PIX_FMT_RGB555;
		nYUVSize	=	_width*_height*2+1;
	}
	else if (renderFormat == D3D_FORMAT_A8R8G8B8)			//21 ok
	{
		nDecoder	= FFC_PIX_FMT_A8R8G8B8;
		nYUVSize	=	_width*_height*4+1;
	}
	else if (renderFormat == (D3D_SUPPORT_FORMAT)D3D_FORMAT_X8R8G8B8)		//22	ok
	{
		nDecoder	= FFC_PIX_FMT_X8R8G8B8;
		nYUVSize	=	_width*_height*4+1;
	}
	else if (renderFormat == GDI_FORMAT_RGB24)
	{
		nDecoder	= FFC_PIX_FMT_RGB24;
		nYUVSize	=	_width*_height*3+1;
	}
	else
	{
		nDecoder	= FFC_PIX_FMT_X8R8G8B8;
		nYUVSize	=	_width*_height*4+1;
	}

	if (NULL != _decoder)	*_decoder = nDecoder;
	if (NULL != yuvsize)	*yuvsize  = nYUVSize;
}

NvDecoder_CodecType FFmpeg2NvCodecId(int codecId)
{
//#define SKEYE_SDK_VIDEO_CODEC_H264	0x1C		/* H264  */
//#define SKEYE_SDK_VIDEO_CODEC_H265	0x48323635	/* 1211250229 */
//#define	SKEYE_SDK_VIDEO_CODEC_MJPEG	0x08		/* MJPEG */
//#define	SKEYE_SDK_VIDEO_CODEC_MPEG4	0x0D		/* MPEG4 */
	switch (codecId)
	{
	case 1:   return NvDecoder_Codec_MPEG1;
	case 2:   return NvDecoder_Codec_MPEG2;
	case SKEYE_SDK_VIDEO_CODEC_MPEG4:  return NvDecoder_Codec_MPEG4;
	case 70:  return NvDecoder_Codec_VC1;
	case SKEYE_SDK_VIDEO_CODEC_H264:  return NvDecoder_Codec_H264;
	case SKEYE_SDK_VIDEO_CODEC_H265: return NvDecoder_Codec_HEVC;
	case 139: return NvDecoder_Codec_VP8;
	case 167: return NvDecoder_Codec_VP9;
	case SKEYE_SDK_VIDEO_CODEC_MJPEG:   return NvDecoder_Codec_JPEG;
	default:  return NvDecoder_Codec_NumCodecs;
	}
}

DECODER_OBJ	*GetDecoder(PLAY_THREAD_OBJ	*_pPlayThread, unsigned int mediaType, MEDIA_FRAME_INFO *_frameinfo)
{
	if (NULL == _pPlayThread || NULL==_frameinfo)								
		return NULL;
	int iIdx = -1;
	int iExist = -1;
	for (int i=0; i<MAX_DECODER_NUM; i++)
	{
		if (MEDIA_TYPE_VIDEO == mediaType)
		{
			if (_frameinfo->width < 1 || _frameinfo->height<1 || _frameinfo->codec<1)		return NULL;

			if (_pPlayThread->decoderObj[i].codec.vidCodec == 0x00 &&
				_pPlayThread->decoderObj[i].codec.width == 0x00 &&
				_pPlayThread->decoderObj[i].codec.height== 0x00 && iIdx==-1)
			{
				iIdx = i;
			}

			if (_pPlayThread->decoderObj[i].codec.vidCodec == _frameinfo->codec &&
				_pPlayThread->decoderObj[i].codec.width == _frameinfo->width &&
				_pPlayThread->decoderObj[i].codec.height== _frameinfo->height)
			{
				if (NULL == _pPlayThread->decoderObj[i].ffDecoder)
				{
					iExist = i;
					int nDecoder = OUTPUT_PIX_FMT_YUV420P;
					//ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, _pPlayThread->renderFormat, &_pPlayThread->decoderObj[i].yuv_size);

					if (_pPlayThread->renderFormat == GDI_FORMAT_RGB24)
					{
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, _pPlayThread->renderFormat, &_pPlayThread->decoderObj[i].yuv_size);
					}
					else
					{
#ifdef __ENABLE_SSE
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, DISPLAY_FORMAT_YV12, &_pPlayThread->decoderObj[i].yuv_size);
#else
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, _pPlayThread->renderFormat, &_pPlayThread->decoderObj[i].yuv_size);
#endif
					}

					FFD_Init(&_pPlayThread->decoderObj[i].ffDecoder);
					int nCodec = CODEC_H264;
					switch (_frameinfo->codec)
					{
					case SKEYE_SDK_VIDEO_CODEC_H264:
						nCodec = CODEC_H264;
						break;
					case SKEYE_SDK_VIDEO_CODEC_H265:
						nCodec = CODEC_H265;
						break;
					case SKEYE_SDK_VIDEO_CODEC_MJPEG:
						nCodec = CODEC_MJPEG;
						break;
					case SKEYE_SDK_VIDEO_CODEC_MPEG4:
						nCodec = CODEC_MPEG4;
						break;
					}
					FFD_SetVideoDecoderParam(_pPlayThread->decoderObj[i].ffDecoder, _frameinfo->width, _frameinfo->height, /*_frameinfo->codec*/nCodec, nDecoder,1);
				}
				if (NULL == _pPlayThread->decoderObj[i].pNvDecoder && _pPlayThread->decoderObj[i].nHardDecode == 1)
				{
					//Nv硬件解码初始化
					int errCode;
					std::string erroStr;
					//int ret = NvDecoder_Initsize(erroStr);
					//if(ret > 0)
					if (pFuncNvDecoder_Create)
					{
						_pPlayThread->decoderObj[i].pNvDecoder = pFuncNvDecoder_Create(FFmpeg2NvCodecId(_frameinfo->codec), _frameinfo->width, _frameinfo->height, false,
							(_pPlayThread->renderFormat == D3D_FORMAT_YV12 || _pPlayThread->renderFormat == GDI_FORMAT_RGB24) ? false : true,
							(_pPlayThread->renderFormat == D3D_FORMAT_X8R8G8B8 || _pPlayThread->renderFormat == D3D_FORMAT_A8R8G8B8) ? bgra : native/*bgra*/, errCode, erroStr);
					}
				}
				else if (NULL == _pPlayThread->decoderObj[i].pIntelDecoder && _pPlayThread->decoderObj[i].nHardDecode == 2)
				{
					//硬件解码初始化
					_pPlayThread->decoderObj[iIdx].pIntelDecoder = Create_IntelHardDecoder();
					int nRet =_pPlayThread->decoderObj[iIdx].pIntelDecoder->Init(_pPlayThread->hWnd, _pPlayThread->decoderObj[i].bInnerShow );
					if (nRet<0)
					{
						Release_IntelHardDecoder(_pPlayThread->decoderObj[iIdx].pIntelDecoder);
						_pPlayThread->decoderObj[iIdx].pIntelDecoder = NULL;
					}
					_pPlayThread->renderFormat = D3D_FORMAT_NV12;
					_pPlayThread->decoderObj[iIdx].yuv_size = _frameinfo->width* _frameinfo->height*3/2+1;

				}
				return &_pPlayThread->decoderObj[i];
			}

			if (iIdx>=0)
			{
				if (NULL == _pPlayThread->decoderObj[i].pNvDecoder && _pPlayThread->decoderObj[i].nHardDecode == 1)
				{
					//Nv硬件解码初始化
					int errCode;
					std::string erroStr;
					if (pFuncNvDecoder_Create)
					{
						_pPlayThread->decoderObj[i].pNvDecoder = pFuncNvDecoder_Create(FFmpeg2NvCodecId(_frameinfo->codec), _frameinfo->width, _frameinfo->height, false,
							(_pPlayThread->renderFormat == D3D_FORMAT_YV12 || _pPlayThread->renderFormat == GDI_FORMAT_RGB24) ? false : true,
							(_pPlayThread->renderFormat == D3D_FORMAT_X8R8G8B8 || _pPlayThread->renderFormat == D3D_FORMAT_A8R8G8B8) ? bgra : native/*bgra*/, errCode, erroStr);
					}
				}
				else if (NULL == _pPlayThread->decoderObj[i].pIntelDecoder && _pPlayThread->decoderObj[i].nHardDecode == 2)
				{
					//硬件解码初始化
					_pPlayThread->decoderObj[iIdx].pIntelDecoder = Create_IntelHardDecoder();
					int nRet =_pPlayThread->decoderObj[iIdx].pIntelDecoder->Init(_pPlayThread->hWnd, _pPlayThread->decoderObj[i].bInnerShow);
					if (nRet<0)
					{
						Release_IntelHardDecoder(_pPlayThread->decoderObj[iIdx].pIntelDecoder);
						_pPlayThread->decoderObj[iIdx].pIntelDecoder = NULL;
					}
					_pPlayThread->renderFormat = D3D_FORMAT_NV12;
					_pPlayThread->decoderObj[iIdx].yuv_size = _frameinfo->width* _frameinfo->height*3/2+1;
				}

				if (NULL == _pPlayThread->decoderObj[iIdx].ffDecoder)
				{
					int nDecoder = OUTPUT_PIX_FMT_YUV420P;
					if (_pPlayThread->renderFormat == GDI_FORMAT_RGB24)
					{
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, _pPlayThread->renderFormat, &_pPlayThread->decoderObj[iIdx].yuv_size);
					}
					else
					{
#ifdef __ENABLE_SSE
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, DISPLAY_FORMAT_YV12, &_pPlayThread->decoderObj[iIdx].yuv_size);
#else
						ParseDecoder2Render(&nDecoder, _frameinfo->width, _frameinfo->height, _pPlayThread->renderFormat, &_pPlayThread->decoderObj[iIdx].yuv_size);
#endif
					}

					FFD_Init(&_pPlayThread->decoderObj[iIdx].ffDecoder);
					int nCodec = CODEC_H264;
					switch (_frameinfo->codec)
					{
					case SKEYE_SDK_VIDEO_CODEC_H264:
						nCodec = CODEC_H264;
						break;
					case SKEYE_SDK_VIDEO_CODEC_H265:
						nCodec = CODEC_H265;
						break;
					case SKEYE_SDK_VIDEO_CODEC_MJPEG:
						nCodec = CODEC_MJPEG;
						break;
					case SKEYE_SDK_VIDEO_CODEC_MPEG4:
						nCodec = CODEC_MPEG4;
						break;
					}
					FFD_SetVideoDecoderParam(_pPlayThread->decoderObj[iIdx].ffDecoder, _frameinfo->width, _frameinfo->height,/* _frameinfo->codec*/nCodec, nDecoder,1);

					if (NULL != _pPlayThread->decoderObj[iIdx].ffDecoder )
					{
						EnterCriticalSection(&_pPlayThread->crit);
						_pPlayThread->resetD3d	=	true;
						LeaveCriticalSection(&_pPlayThread->crit);

						_pPlayThread->decoderObj[iIdx].codec.vidCodec	= _frameinfo->codec;
						_pPlayThread->decoderObj[iIdx].codec.width	= _frameinfo->width;
						_pPlayThread->decoderObj[iIdx].codec.height = _frameinfo->height;
						
						return &_pPlayThread->decoderObj[iIdx];
					}
					else
					{
						return NULL;
					}
				}

			}
		}
		else if (MEDIA_TYPE_AUDIO == mediaType)
		{
			if (_frameinfo->sample_rate < 1 || _frameinfo->channels<1 || _frameinfo->codec<1)		return NULL;

			if (_pPlayThread->decoderObj[i].codec.audCodec == 0x00 &&
				_pPlayThread->decoderObj[i].codec.samplerate == 0x00 &&
				_pPlayThread->decoderObj[i].codec.channels== 0x00 && iIdx==-1)
			{
				iIdx = i;
			}

			if (_pPlayThread->decoderObj[i].codec.audCodec == _frameinfo->codec &&
				_pPlayThread->decoderObj[i].codec.samplerate == _frameinfo->sample_rate &&
				_pPlayThread->decoderObj[i].codec.channels== _frameinfo->channels)
			{
				if (NULL == _pPlayThread->decoderObj[i].ffDecoder)
				{
					iExist = i;

					FFD_Init(&_pPlayThread->decoderObj[i].ffDecoder);
					FFD_SetAudioDecoderParam(_pPlayThread->decoderObj[i].ffDecoder, _frameinfo->channels, 
						_frameinfo->sample_rate, _frameinfo->bits_per_sample, _frameinfo->codec);
				}
				return &_pPlayThread->decoderObj[i];
			}

			if (iIdx>=0)
			{
				if (NULL == _pPlayThread->decoderObj[iIdx].ffDecoder)
				{
					FFD_Init(&_pPlayThread->decoderObj[iIdx].ffDecoder);
					FFD_SetAudioDecoderParam(_pPlayThread->decoderObj[i].ffDecoder, _frameinfo->channels, _frameinfo->sample_rate, _frameinfo->bits_per_sample, _frameinfo->codec);

					if (NULL != _pPlayThread->decoderObj[iIdx].ffDecoder)
					{
						_pPlayThread->decoderObj[iIdx].codec.audCodec	= _frameinfo->codec;
						_pPlayThread->decoderObj[iIdx].codec.samplerate	= _frameinfo->sample_rate;
						_pPlayThread->decoderObj[iIdx].codec.channels = _frameinfo->channels;
						_pPlayThread->decoderObj[iIdx].codec.bits_per_sample = _frameinfo->bits_per_sample;
					
						return &_pPlayThread->decoderObj[iIdx];
					}
					else
					{
						return NULL;
					}
				}
				else
				{
					FFD_SetAudioDecoderParam(_pPlayThread->decoderObj[i].ffDecoder, _frameinfo->channels, _frameinfo->sample_rate, _frameinfo->bits_per_sample, _frameinfo->codec);
					_pPlayThread->decoderObj[iIdx].codec.audCodec	= _frameinfo->codec;
					_pPlayThread->decoderObj[iIdx].codec.samplerate	= _frameinfo->sample_rate;
					_pPlayThread->decoderObj[iIdx].codec.channels = _frameinfo->channels;
					_pPlayThread->decoderObj[iIdx].codec.bits_per_sample = _frameinfo->bits_per_sample;

					return &_pPlayThread->decoderObj[iIdx];
				}
			}
		}
	}

	return NULL;
}

LPTHREAD_START_ROUTINE CChannelManager::_lpDecodeThread( LPVOID _pParam )
{
	PLAY_THREAD_OBJ *pThread = (PLAY_THREAD_OBJ*)_pParam;
	if (NULL == pThread)		
		return 0;
	pThread->decodeThread.flag	=	0x02;

#ifdef _DEBUG
	_TRACE("解码线程[%d]已启动. ThreadId:%d ...\n", pThread->channelId, GetCurrentThreadId());
#endif

	//SetThreadAffinityMask(GetCurrentThread(), 1);

	if (NULL != pThread->yuvFrame)
	{
		for (int i=0; i<MAX_YUV_FRAME_NUM; i++)
		{
			memset(&pThread->yuvFrame[i].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
		}
	}
	unsigned int channelid = 0;
	unsigned int mediatype = 0;
	MEDIA_FRAME_INFO	frameinfo;
	//int buf_size = 1024*1024;
	int buf_size = 3840*2080*4;

	char *pbuf = new char[buf_size];
	if (NULL == pbuf)
	{
		pThread->decodeThread.flag	=	0x00;
		return 0;
	}
	memset(pbuf, 0x00, buf_size);

	Skeye_Handle m_pAACEncoderHandle = NULL;
	char* m_pAACEncBufer = new char[buf_size];
	memset(m_pAACEncBufer, 0x00, buf_size);

	pThread->decodeYuvIdx	=	0;
	memset(&frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));

	//#define AVCODEC_MAX_AUDIO_FRAME_SIZE	(192000)
#define AVCODEC_MAX_AUDIO_FRAME_SIZE	(64000)
	int audbuf_len = (AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2;
	unsigned char *audio_buf = new unsigned char[audbuf_len+1];
	memset(audio_buf, 0x00, audbuf_len);
	unsigned long decTimeStamp = 0;
	//FILE *fES = fopen("test.h264", "wb");

	while (1)
	{
		if (pThread->decodeThread.flag == 0x03)	
			break;
		if (pThread->initQueue == 0x00 || NULL==pThread->pAVQueue)
		{
			Sleep(1);
			continue;
		}
		int ret = SSQ_GetData(pThread->pAVQueue, &channelid, &mediatype, &frameinfo, pbuf);
		if (ret < 0)
		{
			_VS_BEGIN_TIME_PERIOD(1);
			__VS_Delay(1);
			_VS_END_TIME_PERIOD(1);
			continue;
		}

		if (mediatype == MEDIA_TYPE_VIDEO)
		{
#ifdef _DEBUG1
			_TRACE("解码线程[%d]解码...%d   %d x %d\n", pThread->id, channelid, frameinfo.width, frameinfo.height);
#endif
			//==============================================

			//if (NULL != fES)
   //         {
   //             fwrite(pbuf, 1, frameinfo.length, fES);
   //         }

			//_TRACE("DECODE queue: %d\n", pChannelObj->pQueue->pQueHeader->videoframes);
			if (pThread->frameQueue > MAX_CACHE_FRAME)
			{
				//_TRACE("[ch%d]缓存帧数[%d]>设定帧数[%d].  清空队列并等待下一个Key frame.\n", pThread->renderCh, pThread->framequeue, MAX_CACHE_FRAME);
				_TRACE("[ch%d]缓存帧数[%d]>设定帧数[%d].  清空队列并等待下一个Key frame.\n", pThread->channelId, pThread->frameQueue, MAX_CACHE_FRAME);

				SSQ_Clear(pThread->pAVQueue);
				pThread->findKeyframe = 0x01;
				pThread->frameQueue = pThread->pAVQueue->pQueHeader->videoframes;
				continue;

				EnterCriticalSection(&pThread->crit);
				SSQ_Clear(pThread->pAVQueue);
				pThread->rtpTimestamp = 0;
				pThread->decodeYuvIdx = 0;
				pThread->findKeyframe = 0x01;
				for (int i=0; i<MAX_YUV_FRAME_NUM; i++)
				{
					memset(&pThread->yuvFrame[i].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
				}
				LeaveCriticalSection(&pThread->crit);
			}

			if ( (pThread->findKeyframe==0x01) && (frameinfo.type==SKEYE_SDK_VIDEO_FRAME_I) )
			{
				pThread->findKeyframe = 0x00;
			}
			else if (pThread->findKeyframe==0x01)
			{
#ifdef _DEBUG
				_TRACE("[ch%d]Find Keyframe..\n", pThread->channelId);
#endif
				continue;
			}

			DECODER_OBJ *pDecoderObj = GetDecoder(pThread, MEDIA_TYPE_VIDEO, &frameinfo);		//获取相应的解码器
			if (NULL == pDecoderObj )
			{
#ifdef _DEBUG
				_TRACE("[ch%d]获取解码器失败.  %d x %d\n", pThread->channelId, frameinfo.width, frameinfo.height);
#endif
				_VS_BEGIN_TIME_PERIOD(1);
				__VS_Delay(1);
				_VS_END_TIME_PERIOD(1);
				continue;
			}
			if (pDecoderObj->yuv_size <= 0)
			{
#ifdef _DEBUG
				_TRACE("[ch%d]获取解码器失败.  %d x %d\n", pThread->channelId, frameinfo.width, frameinfo.height);
#endif
				_VS_BEGIN_TIME_PERIOD(1);
				__VS_Delay(1);
				_VS_END_TIME_PERIOD(1);
				continue;
			}

			//如果已申请的内存小于当前需要的内存大小,则重新申请
			if ( (pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize < pDecoderObj->yuv_size) && (NULL != pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf))
			{
				__DELETE_ARRAY(pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf);
			}
			if (NULL == pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf && pDecoderObj->yuv_size>0)
			{

				pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize = pDecoderObj->yuv_size;//MAX_FRAME_SIZE;//pThread->yuv_size;
				_TRACE("[pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize = %d ...\n", pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize);
				pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf = new char[pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize];
			}
			if (NULL == pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf)	
				continue;

			while (pThread->yuvFrame[pThread->decodeYuvIdx].frameinfo.length > 0)		//等待该帧显示完成
			{
				if (pThread->decodeThread.flag == 0x03)		break;

				_VS_BEGIN_TIME_PERIOD(1);
				__VS_Delay(1);
				_VS_END_TIME_PERIOD(1);
			}
			if (pThread->decodeThread.flag == 0x03)	
				break;

			if (frameinfo.timestamp_sec == 0 && frameinfo.timestamp_usec == 0)
			{
				decTimeStamp += (1000 / frameinfo.fps);
			}
			else
			{
				decTimeStamp = frameinfo.timestamp_sec * 1000 + frameinfo.timestamp_usec / 1000;
			}
			_TRACE("[decTimeStamp = %d \n", decTimeStamp);
			//录像
			if (pThread->manuRecording == 0x01 && NULL==pThread->m_pMP4Writer && frameinfo.type==SKEYE_SDK_VIDEO_FRAME_I)//开启录制
			{
				if (!pThread->m_pMP4Writer)
				{
					pThread->m_pMP4Writer = new SkeyeMP4Writer();
				}

				unsigned int timestamp = (unsigned int)time(NULL);
				time_t tt = timestamp;
				struct tm *_time = localtime(&tt);
				char szTime[64] = {0,};
				strftime(szTime, 32, "%Y%m%d%H%M%S", _time);

				int nRecordPathLen = strlen(pThread->manuRecordingPath);
				if (nRecordPathLen==0 || (pThread->manuRecordingPath[nRecordPathLen-1] != '/' && pThread->manuRecordingPath[nRecordPathLen-1] != '\\') )
				{
					pThread->manuRecordingPath[nRecordPathLen] = '/';
				}
				
				char sFileName[512] = {0,};
				sprintf(sFileName, "%sch%d_%s.mp4", pThread->manuRecordingPath, pThread->channelId, szTime);
				 // [10/10/2016 SwordTwelve]
				if (!pThread->m_pMP4Writer->CreateMP4File(sFileName,  ZOUTFILE_FLAG_FULL))//ZOUTFILE_FLAG_VIDEO
				{
					delete pThread->m_pMP4Writer;
					pThread->m_pMP4Writer = NULL;
				}		
				else
				{
				}
			}
			if ( NULL != pThread->m_pMP4Writer)//录制
			{
				if (pThread->manuRecording == 0x00 )//停止录制
				{
					pThread->m_pMP4Writer->SaveFile();
					delete pThread->m_pMP4Writer;
					pThread->m_pMP4Writer=NULL;
				} 
				else//继续MP4写数据
				{

					pThread->m_pMP4Writer->WriteMp4File((unsigned char*)pbuf, frameinfo.length, 
						(frameinfo.type==SKEYE_SDK_VIDEO_FRAME_I)?true:false, 
						decTimeStamp, frameinfo.width, frameinfo.height);

				}
			}

			// 不再支持MP4Creator录制mp4（这个库容易出现崩溃） [9/20/2016 dingshuai]
#if 0
			//手动录像
			if (NULL != pThread->mp4cHandle)
			{
				//30fps * 60 seconds * 30 minutes  = 手动录像必须小于30分钟
				if (pThread->vidFrameNum >= 30*60*30 || (pThread->manuRecording == 0x00) )
				{
					MP4C_CloseMp4File(pThread->mp4cHandle);
					MP4C_Deinit(&pThread->mp4cHandle);
					pThread->mp4cHandle = NULL;
					pThread->vidFrameNum = 0;
				}
				else
				{
					pThread->vidFrameNum ++;
					MP4C_AddFrame(pThread->mp4cHandle, MEDIA_TYPE_VIDEO, (unsigned char*)pbuf, frameinfo.length, frameinfo.type, frameinfo.timestamp_sec, frameinfo.timestamp_sec*1000+frameinfo.timestamp_usec/1000, frameinfo.fps);
				}
			}
			else if (pThread->manuRecording == 0x01 && NULL==pThread->mp4cHandle)
			{
				unsigned int timestamp = (unsigned int)time(NULL);
				time_t tt = timestamp;
				struct tm *_time = localtime(&tt);
				char szTime[64] = {0,};
				strftime(szTime, 32, "%Y%m%d %H%M%S", _time);

				memset(pThread->manuRecordingFile, 0x00, sizeof(pThread->manuRecordingFile));
				sprintf(pThread->manuRecordingFile, "ch%d_%s.mp4", pThread->channelId, szTime);
				//sprintf(pThread->manuRecordingFile, "ch%d.mp4", pThread->channelId);

				MP4C_Init(&pThread->mp4cHandle);
				MP4C_SetMp4VideoInfo(pThread->mp4cHandle, VIDEO_CODEC_H264, frameinfo.width, frameinfo.height, frameinfo.fps);
				MP4C_SetMp4AudioInfo(pThread->mp4cHandle, AUDIO_CODEC_AAC, 8000, 1);
				MP4C_CreateMp4File(pThread->mp4cHandle, pThread->manuRecordingFile, 1024*1024*2);
			}
#endif

			if (pThread->decodeKeyFrameOnly == 0x01)
			{
				if (frameinfo.type != SKEYE_SDK_VIDEO_FRAME_I)
				{
					pThread->findKeyframe = 0x01;
					continue;
				}
			}

			//解码
			EnterCriticalSection(&pThread->crit);
			int nRet = 1;

			if (pDecoderObj->pNvDecoder)
			{
				if (frameinfo.length > 0&& pFuncNvDecoder_Decode)
				{
					uint8_t **ppFrame;
					int nFrameReturned = 0;
					int nFrameLen = 0;

					int nRet = pFuncNvDecoder_Decode(pDecoderObj->pNvDecoder, (const uint8_t*)pbuf, frameinfo.length, &ppFrame, &nFrameLen, &nFrameReturned);
					if (nRet<0)
					{
						//std::ostringstream err;
						//err << "Decode ret: " << ret << std::endl;
						//throw std::invalid_argument(err.str());
						//continue;
					}
					else //硬件解码成功
					{
						for (int i = 0; i < nFrameReturned; i++)
						{
							//int WH = frameInfo.width * ((frameInfo.height * 3) >> 1);
							if (NULL == pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf)
							{
								//BUFF_MALLOC(&pThread->yuvFrame[pThread->decodeYuvIdx].yuvBuf, pDecoderObj->yuvSize);
								continue;
							}

							if (0 == pThread->yuvFramePlannelSize ||
								pThread->yuvFrameWidth != frameinfo.width ||
								pThread->yuvFrameHeight != frameinfo.height)
							{
								pThread->yuvFramePlannelSize = frameinfo.width*frameinfo.height;
							}

							// 数据格式转换 [2019/08/04 SwordTwelve]
							if (D3D_FORMAT_YV12 == pThread->renderFormat)
							{
								//int NV12ToI420(const uint8_t* src_y,
							//	int src_stride_y,
							//	const uint8_t* src_uv,
							//	int src_stride_uv,
							//	uint8_t* dst_y,
							//	int dst_stride_y,
							//	uint8_t* dst_u,
							//	int dst_stride_u,
							//	uint8_t* dst_v,
							//	int dst_stride_v,
							//	int width,
							//	int height)
								unsigned char* src_y = ppFrame[i];
								int src_y_stride = frameinfo.width;
								unsigned char* src_vu = ppFrame[i] + pThread->yuvFramePlannelSize;
								int src_vu_stride = (frameinfo.width + 1) / 2 * 2;

								int dst_stride_y = frameinfo.width;
								int dst_stride_u = (frameinfo.width + 1) / 2;
								int dst_stride_v = dst_stride_u;
								int dst_y_size = pThread->yuvFramePlannelSize;
								int dst_u_size = (pThread->yuvFramePlannelSize >> 2);
								unsigned char *dst_i420_y_data = (unsigned char *)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf;
								unsigned char *dst_i420_u_data = (unsigned char *)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf + dst_y_size;
								unsigned char *dst_i420_v_data = (unsigned char *)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf + dst_y_size + dst_u_size;

								/*							_TRACE("[pThread->decodeYuvIdx = …%d,  frameinfo.width = …%d, frameinfo.height = …%d \
															pThread->yuvFramePlannelSize = %d  pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf = %08x\n", pThread->decodeYuvIdx, frameinfo.width, frameinfo.height, pThread->yuvFramePlannelSize, pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf);*/

								libyuv::NV12ToI420(src_y, src_y_stride, src_vu, src_vu_stride,
									dst_i420_y_data, dst_stride_y,
									dst_i420_u_data, dst_stride_u,
									dst_i420_v_data, dst_stride_v,
									frameinfo.width, frameinfo.height);
							}
//							else if (D3D_FORMAT_NV12 == pThread->renderFormat)
//							{
//#if 0
//								//unsigned char* src_y = ppFrame[i];
//								//int src_y_stride = frameInfo.width;
//								//unsigned char* src_vu = ppFrame[i] + pMediaChannel->yuvFramePlannelSize;
//								//libyuv::NV12ToARGB
//
//								//YV12->ARGB
//								//int src_vu_stride = (frameInfo.width + 1) / 2 * 2;
//								libyuv::I420ToARGB(dst_i420_y_data, dst_stride_y,
//									dst_i420_u_data, dst_stride_u,
//									dst_i420_v_data, dst_stride_v, (unsigned char*)pMediaChannel->yuvFrame[pMediaChannel->decodeYuvIdx].yuvBuf.pbuf, frameInfo.width << 2, frameInfo.width, frameInfo.height);
//#else
//								if (NULL == pTmpBuffer)
//								{
//									pTmpBuffer = new unsigned char[pThread->yuvFramePlannelSize * 3 / 2 + 1];
//
//								}
//								libyuv_nv12toargb(ppFrame[i], (unsigned char*)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf, pTmpBuffer, pMediaChannel->yuvFramePlannelSize, frameInfo.width, frameInfo.height);
//#endif
//							}
							else if (GDI_FORMAT_RGB24 == pThread->renderFormat)
							{
								unsigned char* src_y = ppFrame[i];
								int src_y_stride = frameinfo.width;
								unsigned char* src_vu = ppFrame[i] + pThread->yuvFramePlannelSize;
								int src_vu_stride = (frameinfo.width + 1) / 2 * 2;

								//YV12->RGB24
								//int src_vu_stride = (frameInfo.width + 1) / 2 * 2;
								libyuv::NV12ToRGB24(src_y, src_y_stride, src_vu, src_vu_stride, 
									(unsigned char*)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf,
									frameinfo.width * 3, frameinfo.width, frameinfo.height);
							}
							else
							{
								memcpy(pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf, ppFrame[i],
									pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize);
							}
																													//抓图
							if (pThread->manuScreenshot == 0x01)//Just support jpg，png
							{
								unsigned int timestamp = (unsigned int)time(NULL);
								time_t tt = timestamp;
								struct tm *_time = localtime(&tt);
								char szTime[64] = { 0, };
								strftime(szTime, 32, "%Y%m%d-%H%M%S", _time);

								// 						char strPath[512] = {0,};
								// 						sprintf(strPath , "%sch%d_%s.jpg", pThread->strScreenCapturePath, pThread->channelId, szTime) ;

								PhotoShotThreadInfo* pShotThreadInfo = new PhotoShotThreadInfo;
								sprintf(pShotThreadInfo->strPath, "%sch%d_%s.jpg", pThread->strScreenCapturePath, pThread->channelId, szTime);

								int nYuvBufLen = frameinfo.width*frameinfo.height * 3;// most size = RGB24, we donot support RGBA Render type
								pShotThreadInfo->pYuvBuf = new unsigned char[nYuvBufLen];
								pShotThreadInfo->width = frameinfo.width;
								pShotThreadInfo->height = frameinfo.height;
								pShotThreadInfo->renderFormat = pThread->renderFormat;

								memcpy(pShotThreadInfo->pYuvBuf, pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf, pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize - 1);
								CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_lpPhotoShotThread, pShotThreadInfo, 0, NULL);
								pThread->manuScreenshot = 0;
							}

							memcpy(&pThread->yuvFrame[pThread->decodeYuvIdx].frameinfo, &frameinfo, sizeof(MEDIA_FRAME_INFO));
							pThread->decodeYuvIdx++;
							if (pThread->decodeYuvIdx >= MAX_YUV_FRAME_NUM)
								pThread->decodeYuvIdx = 0;
						}
					}
				}
			}
			else if (pDecoderObj->pIntelDecoder)//硬件解码
			{
				unsigned char* outPutYUV = (unsigned char*)pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf;
				nRet = pDecoderObj->pIntelDecoder->Decode((unsigned char*)pbuf, frameinfo.length, (unsigned char*)outPutYUV);	
				//if (nRet>=0)
				{
					memcpy(&pThread->yuvFrame[pThread->decodeYuvIdx].frameinfo, &frameinfo, sizeof(MEDIA_FRAME_INFO));
					pThread->decodeYuvIdx ++;
					if (pThread->decodeYuvIdx >= MAX_YUV_FRAME_NUM)		
						pThread->decodeYuvIdx = 0;
				}
			}
			else
			{
				nRet = FFD_DecodeVideo3(pDecoderObj->ffDecoder, pbuf, frameinfo.length, pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf, frameinfo.width, frameinfo.height);
				if (0 != nRet)
				{
					if (nRet == -4)//-4表示为当前帧尚未解码完成，不作为错误判断
					{
						_TRACE("视频帧解码尚未完成[%d]... framesize:%d   %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n", nRet, frameinfo.length,
							(unsigned char)pbuf[0], (unsigned char)pbuf[1], (unsigned char)pbuf[2], (unsigned char)pbuf[3], (unsigned char)pbuf[4],
							(unsigned char)pbuf[5], (unsigned char)pbuf[6], (unsigned char)pbuf[7], (unsigned char)pbuf[8], (unsigned char)pbuf[9]);
					}
					else
					{
						_TRACE("解码失败... framesize:%d   %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\n", frameinfo.length,
							(unsigned char)pbuf[0], (unsigned char)pbuf[1], (unsigned char)pbuf[2], (unsigned char)pbuf[3], (unsigned char)pbuf[4],
							(unsigned char)pbuf[5], (unsigned char)pbuf[6], (unsigned char)pbuf[7], (unsigned char)pbuf[8], (unsigned char)pbuf[9]);

						if (frameinfo.type == SKEYE_SDK_VIDEO_FRAME_I)		//关键帧
						{
							_TRACE("[ch%d]当前关键帧解码失败...\n", pThread->channelId);
#ifdef _DEBUG
							FILE *f = fopen("keyframe.txt", "wb");
							if (NULL != f)
							{
								fwrite(pbuf, 1, frameinfo.length, f);
								fclose(f);
							}
#endif
							pThread->findKeyframe = 0x01;

						}
						else
						{
#ifdef _DEBUG
							FILE *f = fopen("pframe.txt", "wb");
							if (NULL != f)
							{
								fwrite(pbuf, 1, frameinfo.length, f);
								fclose(f);
							}
#endif
						}
					}
				}
				else
				{
					memcpy(&pThread->yuvFrame[pThread->decodeYuvIdx].frameinfo, &frameinfo, sizeof(MEDIA_FRAME_INFO));

					pThread->decodeYuvIdx ++;
					if (pThread->decodeYuvIdx >= MAX_YUV_FRAME_NUM)		pThread->decodeYuvIdx = 0;

					//抓图
					if (pThread->manuScreenshot == 0x01 )//&& pThread->renderFormat == D3D_FORMAT_YUY2)//Just support YUY2->jpg
					{
						unsigned int timestamp = (unsigned int)time(NULL);
						time_t tt = timestamp;
						struct tm *_time = localtime(&tt);
						char szTime[64] = {0,};
						strftime(szTime, 32, "%Y%m%d-%H%M%S", _time);
	
						PhotoShotThreadInfo* pShotThreadInfo = new PhotoShotThreadInfo;
						sprintf(pShotThreadInfo->strPath , "%sch%d_%s.jpg", pThread->strScreenCapturePath, pThread->channelId, szTime) ;

						int nYuvBufLen = pThread->yuvFrame[pThread->decodeYuvIdx].Yuvsize;// frameinfo.width*frameinfo.height << 1;
						pShotThreadInfo->pYuvBuf = new unsigned char[nYuvBufLen];
						pShotThreadInfo->width = frameinfo.width;
						pShotThreadInfo->height = frameinfo.height;

						memcpy(pShotThreadInfo->pYuvBuf, pThread->yuvFrame[pThread->decodeYuvIdx].pYuvBuf, nYuvBufLen);
						CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_lpPhotoShotThread, pShotThreadInfo, 0, NULL);
						pThread->manuScreenshot = 0;
					}
				}
			}

			LeaveCriticalSection(&pThread->crit);
		}
		else if (MEDIA_TYPE_AUDIO == mediatype)		//音频
		{
#if 1
			if (NULL != pChannelManager)
			{
				if (NULL != pChannelManager->pAudioPlayThread && pChannelManager->pAudioPlayThread->channelId == pThread->channelId)
				{
					char* pDecBuffer = pbuf;
					unsigned int nDecBufLen = frameinfo.length;
					if (frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_G711U || SKEYE_SDK_AUDIO_CODEC_G726 == frameinfo.codec || frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_G711A) 
					{
						if (!m_pAACEncoderHandle)
						{
							InitParam initParam;
							initParam.u32AudioSamplerate=frameinfo.sample_rate;
							initParam.ucAudioChannel=frameinfo.channels;
							initParam.u32PCMBitSize=frameinfo.bits_per_sample;
							if (frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_G711U)
							{
								initParam.ucAudioCodec = Law_ULaw;
							} 
							else if (frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_G726)
							{
								initParam.ucAudioCodec = Law_G726;
							}
							else if (frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_G711A)
							{
								initParam.ucAudioCodec = Law_ALaw;
							}
							m_pAACEncoderHandle = Skeye_AACEncoder_Init( initParam);
						}
						unsigned int out_len = 0;
						int nRet = Skeye_AACEncoder_Encode(m_pAACEncoderHandle, (unsigned char*)/*m_pG711EncBufer*/pbuf, /*m_nG711BufferLen*/frameinfo.length, (unsigned char*)m_pAACEncBufer, &out_len) ;
						if (nRet>0&&out_len>0)
						{
							pDecBuffer = m_pAACEncBufer;
							nDecBufLen = out_len;
							frameinfo.codec = SKEYE_SDK_AUDIO_CODEC_AAC;
						} 
						else
						{
							continue;
						}
					}

					if (pThread&&pThread->m_pMP4Writer && frameinfo.codec == SKEYE_SDK_AUDIO_CODEC_AAC)//音频写入MP4文件(now we just support AAC)
					{
						if (pThread->m_pMP4Writer->CanWrite())
						{
							pThread->m_pMP4Writer->WriteAACToMp4File((unsigned char*)pbuf, 
								frameinfo.length, frameinfo.timestamp_sec*1000+frameinfo.timestamp_usec/1000, frameinfo.sample_rate, frameinfo.channels, frameinfo.bits_per_sample);
						}
					}

					DECODER_OBJ *pDecoderObj = GetDecoder(pThread, MEDIA_TYPE_AUDIO, &frameinfo);
					if (NULL == pDecoderObj)
					{
#ifdef _DEBUG
						_TRACE("[ch%d]获取音频解码器失败: %d.\n", pThread->channelId, frameinfo.codec);
#endif
						continue;
					}

					memset(audio_buf, 0x00, audbuf_len);
					int pcm_data_size = 0;
					int ret = FFD_DecodeAudio(pDecoderObj->ffDecoder, (char*)pDecBuffer, nDecBufLen, (char *)audio_buf, &pcm_data_size);	//音频解码(支持g711(ulaw)和AAC)
					if (ret == 0)
					{
						//播放
						if (pChannelManager->pAudioPlayThread->audiochannels == 0)
						{
							pChannelManager->SetAudioParams(pDecoderObj->codec.channels, pDecoderObj->codec.samplerate, pDecoderObj->codec.bits_per_sample);//32);// 16);
						}
						if (pChannelManager->pAudioPlayThread->audiochannels != 0 && NULL!=pChannelManager->pAudioPlayThread->pSoundPlayer)
						{
							pChannelManager->pAudioPlayThread->pSoundPlayer->Write((char *)audio_buf, pcm_data_size);
						}
					}
					else
					{
#ifdef _DEBUG
						_TRACE("[ERROR]解码音频失败....\n");
#endif
					}
				}
			}
#endif
		}
		else if (MEDIA_TYPE_EVENT == mediatype)
		{
			if (frameinfo.type == 0xF1)		//Loss Packet
			{
				pThread->dwLosspacketTime	=	GetTickCount();
			}
			else if (frameinfo.type == 0xFF)	//Disconnect
			{
				pThread->dwDisconnectTime	=	GetTickCount();
			}
		}
	}

	if ( (NULL != pChannelManager) && (NULL != pChannelManager->pAudioPlayThread) && (pChannelManager->pAudioPlayThread->channelId == pThread->channelId) )
	{
		if (NULL!=pChannelManager->pAudioPlayThread->pSoundPlayer)
		{
			pChannelManager->pAudioPlayThread->pSoundPlayer->Close();
		}
		pChannelManager->pAudioPlayThread->channelId = -1;
		pChannelManager->pAudioPlayThread->audiochannels = 0;
	}

	if (m_pAACEncoderHandle)
	{
		Skeye_AACEncoder_Release(m_pAACEncoderHandle);
		m_pAACEncoderHandle = NULL;
	}

	if (m_pAACEncBufer)
	{
		delete[] m_pAACEncBufer ;
		m_pAACEncBufer = NULL;
	}

	delete []audio_buf;
	delete []pbuf;
	pbuf = NULL;

  //  if (NULL != fES)  
		//fclose(fES);

	pThread->decodeThread.flag	=	0x00;

#ifdef _DEBUG
	_TRACE("解码线程[%d]已退出 ThreadId:%d.\n", pThread->channelId, GetCurrentThreadId());
#endif

	return 0;
}

////目前支持的色彩格式
//typedef enum _FFCPixel_Format {
//	FFC_PIX_FMT_YUV420P = 0,			//AV_PIX_FMT_YUV420P
//	FFC_PIX_FMT_YUY2 = 1,			//PIX_FMT_YUYV422
//	FFC_PIX_FMT_RGB24 = 2,			//AV_PIX_FMT_RGB24
//	FFC_PIX_FMT_UYVY = 15,			//AV_PIX_FMT_UYVY422
//	FFC_PIX_FMT_A8R8G8B8 = 28,			//PIX_FMT_BGRA
//	FFC_PIX_FMT_X8R8G8B8 = 28,			//PIX_FMT_BGRA
//	FFC_PIX_FMT_RGB565 = 37,			//PIX_FMT_RGB565LE
//	FFC_PIX_FMT_RGB555 = 39,			//PIX_FMT_RGB555LE
//
//}FFCPixel_Format;
// 抓图函数实现
LPTHREAD_START_ROUTINE CChannelManager::_lpPhotoShotThread(LPVOID _pParam)
{
	if (_pParam)
	{
		FFCPixel_Format FFVFormat = FFC_PIX_FMT_RGB24;
		PhotoShotThreadInfo* pThreadInfo = (PhotoShotThreadInfo*)_pParam;
		switch (pThreadInfo->renderFormat)
		{
		case D3D_FORMAT_YUY2:
			FFVFormat = FFC_PIX_FMT_YUY2;
			break;
		case D3D_FORMAT_YV12:
			FFVFormat = FFC_PIX_FMT_YUV420P;
			break;;
		case	D3D_FORMAT_UYVY:
			FFVFormat = FFC_PIX_FMT_UYVY;
			break;
		case	D3D_FORMAT_A8R8G8B8:
			FFVFormat = FFC_PIX_FMT_A8R8G8B8;
			break;
		case	D3D_FORMAT_X8R8G8B8:
			FFVFormat = FFC_PIX_FMT_X8R8G8B8;
			break;
		case	D3D_FORMAT_RGB565:
			FFVFormat = FFC_PIX_FMT_RGB565;
			break;
		case	D3D_FORMAT_RGB555:
			FFVFormat = FFC_PIX_FMT_RGB555;
			break;
		case GDI_FORMAT_RGB24:
			FFVFormat = FFC_PIX_FMT_RGB24;
			break;
		}
		int ret = FFD_TakeSnap(pThreadInfo->strPath, pThreadInfo->width, pThreadInfo->height, pThreadInfo->pYuvBuf, FFVFormat);
		if (ret)
		{
		}

		delete[] pThreadInfo->pYuvBuf;
		delete pThreadInfo;
}
	return 0;
}


LPTHREAD_START_ROUTINE CChannelManager::_lpDisplayThread(LPVOID _pParam)
{
	PLAY_THREAD_OBJ *pThread = (PLAY_THREAD_OBJ*)_pParam;
	if (NULL == pThread)			return 0;

	pThread->displayThread.flag = 0x02;

#ifdef _DEBUG
	_TRACE("显示线程[%d]已启动. ThreadId:%d ...\n", pThread->channelId, GetCurrentThreadId());
#endif

	int width = 0;
	int height = 0;
	RECT	rcVideoRender;
	SetRectEmpty(&rcVideoRender);

	int iInitTimestamp = 0;

	pThread->rtpTimestamp = 0;
	unsigned int deviceLostTime = (unsigned int)time(NULL) - 2;

	int iLastDisplayYuvIdx = -1;

	MEDIA_FRAME_INFO	lastFrameInfo;
	memset(&lastFrameInfo, 0x00, sizeof(MEDIA_FRAME_INFO));

	//#ifdef _DEBUG
#if 1
	float	fDisplayTimes = 0.0f;		//显示耗时统计
	int		displayFrameNum = 0;
	int		nDisplayTotalTimes = 0;
	unsigned int uiLastTotalTime = 0;
#endif

	int	iDropFrame = 0;		//丢帧机制

	int iDelay = 0;

	_VS_BEGIN_TIME_PERIOD(1);
	QueryPerformanceFrequency(&pThread->cpuFreq);
	_VS_END_TIME_PERIOD(1);
	while (1)
	{
		if (pThread->displayThread.flag == 0x03)		break;

		int iDispalyYuvIdx = -1;
		int iYuvFrameNum = 0;
		unsigned int getNextFrame = 0;
		unsigned int rtpTimestamp = pThread->rtpTimestamp;
		do
		{
			if (pThread->displayThread.flag == 0x03)		break;

			for (int iYuvIdx = 0; iYuvIdx < MAX_YUV_FRAME_NUM; iYuvIdx++)
			{
				if (pThread->displayThread.flag == 0x03)		break;
				if (pThread->yuvFrame[iYuvIdx].frameinfo.length < 1)		continue;

				unsigned int timestampTmp = pThread->yuvFrame[iYuvIdx].frameinfo.timestamp_sec * 1000 + pThread->yuvFrame[iYuvIdx].frameinfo.timestamp_usec / 1000;

				if (timestampTmp > rtpTimestamp && getNextFrame == 0)
				{
					rtpTimestamp = timestampTmp;
					iDispalyYuvIdx = iYuvIdx;
					getNextFrame = 1;
				}
				else if (timestampTmp <= rtpTimestamp)
				{
					rtpTimestamp = timestampTmp;
					iDispalyYuvIdx = iYuvIdx;
					//break;
				}
				/*
				else if (pThread->yuvFrame[iYuvIdx].frameinfo.rtptimestamp == rtpTimestamp)
				{
				memset(&pThread->yuvFrame[iYuvIdx].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
				iDispalyYuvIdx = iYuvIdx;
				break;
				}
				*/
				iYuvFrameNum++;
			}
			_VS_BEGIN_TIME_PERIOD(1);
			__VS_Delay(1);
			_VS_END_TIME_PERIOD(1);

		} while (iDispalyYuvIdx == -1);

		pThread->dwDisconnectTime = 0;
		if (pThread->displayThread.flag == 0x03)						break;
		if (iDispalyYuvIdx < 0 || iDispalyYuvIdx >= MAX_YUV_FRAME_NUM)	continue;

		if (pThread->yuvFrame[iDispalyYuvIdx].frameinfo.width < 1 ||
			pThread->yuvFrame[iDispalyYuvIdx].frameinfo.height < 1)
		{
			continue;
		}

#if 0//渲染线程回调模式
		if ((NULL == pThread->hWnd) || (NULL != pThread->hWnd && (!IsWindow(pThread->hWnd))) /*|| (NULL!=pThread->hWnd && (!IsWindowVisible(pThread->hWnd)))*/)
		{
			pThread->rtpTimestamp = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_sec * 1000 + pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_usec / 1000;

			memset(&pThread->yuvFrame[iDispalyYuvIdx].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
			continue;
		}
#endif

#ifdef _DEBUG1
		static unsigned int uiTmpTimestamp = 0;
		if (uiTmpTimestamp == 0)		uiTmpTimestamp = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.rtptimestamp;
		if (pThread->yuvFrame[iDispalyYuvIdx].frameinfo.rtptimestamp <= uiTmpTimestamp)
		{
			_TRACE("当前时间戳[%u] <= 最新的时间戳[%u].\n", pThread->yuvFrame[iDispalyYuvIdx].frameinfo.rtptimestamp, uiTmpTimestamp);
			uiTmpTimestamp = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.rtptimestamp;
		}

#endif


		iLastDisplayYuvIdx = iDispalyYuvIdx;

		pThread->frameQueue = 0;
		if (NULL != pThread->pAVQueue && NULL != pThread->pAVQueue->pQueHeader)
			//if (pThread->initQueue==0x01 && NULL!=pThread->avQueue.pQueHeader)
		{
			pThread->frameQueue = pThread->pAVQueue->pQueHeader->videoframes;
		}

		int iQue1_DecodeQueue = pThread->frameQueue;		//未解码的帧数
		int iQue2_DisplayQueue = iYuvFrameNum;				//已解码的帧数

		int nQueueFrame = iQue1_DecodeQueue + iQue2_DisplayQueue;

		RECT rcSrc;
		RECT rcDst;
		if (!IsRectEmpty(&pThread->rcSrcRender))
		{
			CopyRect(&rcSrc, &pThread->rcSrcRender);
		}
		else
		{
			SetRect(&rcSrc, 0, 0, width, height);
		}
		if (NULL != pThread->hWnd && (IsWindow(pThread->hWnd)))
		{
			GetClientRect(pThread->hWnd, &rcDst);

			//如果当前分辨率和之前的不同,则重新初始化d3d
			if (lastFrameInfo.width != pThread->yuvFrame[iDispalyYuvIdx].frameinfo.width ||
				lastFrameInfo.height != pThread->yuvFrame[iDispalyYuvIdx].frameinfo.height)
			{
				pThread->resetD3d = true;
			}

			EnterCriticalSection(&pThread->crit);			//Lock

			if (pThread->resetD3d)
			{
				pThread->resetD3d = false;
				//关闭d3d
				if (pThread->renderFormat == GDI_FORMAT_RGB24)
				{
#ifdef _WIN64
					GDI_DeinitDraw(&pThread->d3dHandle);
#else
					RGB_DeinitDraw(&pThread->d3dHandle);
#endif
				}
				else
				{
					D3D_Release(&pThread->d3dHandle);
				}
			}
			width = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.width;
			height = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.height;

			LeaveCriticalSection(&pThread->crit);			//Unlock

															//创建D3dRender
			if (pThread->renderFormat == GDI_FORMAT_RGB24)
			{
				if (NULL == pThread->d3dHandle)
				{
#ifdef _WIN64
					GDI_InitDraw(&pThread->d3dHandle);
#else
					RGB_InitDraw(&pThread->d3dHandle);
#endif			
				}
			}
			else if ((NULL == pThread->d3dHandle) && ((unsigned int)time(NULL) - deviceLostTime >= 2))
			{
				D3D_FONT	font;
				memset(&font, 0x00, sizeof(D3D_FONT));
				font.bold = 0x00;
				wcscpy(font.name, TEXT("Arial Black"));
				font.size = (int)(float)((width)*0.015f);// 32;
				if (pThread->showOSD)
				{
					font.size = pThread->osd.size;// 32;
				}
				font.width = (int)(float)((font.size) / 2.5f);//

				if (NULL != pThread->hWnd && (IsWindow(pThread->hWnd)))
				{
					D3D_SUPPORT_FORMAT d3dFormat = pThread->renderFormat;
					if (d3dFormat == D3D_FORMAT_NV12)
					{
						d3dFormat = D3D_FORMAT_YV12;
					}
					D3D_Initial(&pThread->d3dHandle, pThread->hWnd, width, height, 0, 2, d3dFormat, &font);

					D3D_SetDisplayFlag(pThread->d3dHandle, D3D_SHOW_ZONE | D3D_SHOW_SEL_BOX | D3D_SHOW_CENTER_LINE);
#if 0    //for a test
					D3D9_LINE	d3d9Line;
					memset(&d3d9Line, 0x00, sizeof(D3D9_LINE));
					d3d9Line.usLineId = 1;
					strcpy(d3d9Line.strLineName, "Line1");
					d3d9Line.dwColor = RGB(0xff, 0x80, 0x00);
					d3d9Line.uiTotalNodes = 5;
					d3d9Line.pNodes[0].x = 80;
					d3d9Line.pNodes[0].y = 120;
					d3d9Line.pNodes[1].x = 120;
					d3d9Line.pNodes[1].y = 120;
					d3d9Line.pNodes[2].x = 120;
					d3d9Line.pNodes[2].y = 220;
					d3d9Line.pNodes[3].x = 80;
					d3d9Line.pNodes[3].y = 220;
					d3d9Line.pNodes[4].x = 80;
					d3d9Line.pNodes[4].y = 120;
					D3D_AddLine(pThread->d3dHandle, &d3d9Line);

					memset(&d3d9Line, 0x00, sizeof(D3D9_LINE));
					d3d9Line.usLineId = 1;
					strcpy(d3d9Line.strLineName, "Line2");
					d3d9Line.dwColor = RGB(0x00, 0xFF, 0x00);
					d3d9Line.uiTotalNodes = 2;
					d3d9Line.pNodes[0].x = 320;
					d3d9Line.pNodes[0].y = 360;
					d3d9Line.pNodes[1].x = 760;
					d3d9Line.pNodes[1].y = 460;
					D3D_AddLine(pThread->d3dHandle, &d3d9Line);

					memset(&d3d9Line, 0x00, sizeof(D3D9_LINE));
					d3d9Line.usLineId = 1;
					strcpy(d3d9Line.strLineName, "Line3");
					d3d9Line.dwColor = RGB(0x00, 0xFF, 0x00);
					d3d9Line.uiTotalNodes = 2;
					d3d9Line.pNodes[0].x = 320;
					d3d9Line.pNodes[0].y = 460;
					d3d9Line.pNodes[1].x = 760;
					d3d9Line.pNodes[1].y = 460;
					D3D_AddLine(pThread->d3dHandle, &d3d9Line);

					//绘制Buoy测试
					D3D9_BUOY buoy;
					memset(&buoy, 0x00, sizeof(D3D9_BUOY));
					strcpy(buoy.strBuoyName, "buoy123");
					buoy.nodePos.x = 500;
					buoy.nodePos.y = 200;
					strcpy(buoy.strNormalFilePath, "圆形摄像头1.png");
					buoy.usBuoyId = 1;

					buoy.bUseOSD = 1;
					buoy.osdBuoy.rect.left = 1;
					buoy.osdBuoy.rect.top = 1;
					buoy.osdBuoy.rect.right = 200;
					buoy.osdBuoy.rect.bottom = 50;
					wcscpy(buoy.osdBuoy.string, L"240");
					buoy.osdBuoy.alpha = 255;
					buoy.osdBuoy.color = RGB(255, 255, 255);
					wcscpy(buoy.osdBuoy.font.name, L"微软雅黑");
					buoy.osdBuoy.font.size = 26;
					buoy.osdBuoy.font.bold = 0;
					buoy.osdBuoy.font.italic = 0;
					buoy.osdBuoy.font.width = 0;
					D3D_AddBuoy(pThread->d3dHandle, &buoy);

					memset(&buoy, 0x00, sizeof(D3D9_BUOY));
					strcpy(buoy.strBuoyName, "buoy456");
					buoy.nodePos.x = 700;
					buoy.nodePos.y = 300;
					strcpy(buoy.strNormalFilePath, "圆形摄像头1.png");
					buoy.usBuoyId = 1;

					buoy.bUseOSD = 0;
					buoy.osdBuoy.rect.left = 0;
					buoy.osdBuoy.rect.top = 10;
					buoy.osdBuoy.rect.right = 200;
					buoy.osdBuoy.rect.bottom = 50;
					wcscpy(buoy.osdBuoy.string, L"IPC网络摄像头");
					buoy.osdBuoy.alpha = 255;
					buoy.osdBuoy.color = RGB(255, 255, 0);
					wcscpy(buoy.osdBuoy.font.name, L"微软雅黑");
					buoy.osdBuoy.font.size = 26;
					buoy.osdBuoy.font.bold = 0;
					buoy.osdBuoy.font.italic = 0;
					buoy.osdBuoy.font.width = 0;
					D3D_AddBuoy(pThread->d3dHandle, &buoy);
#endif
#if 0
					// 绘制经纬刻度
					for (int i = 0; i < width / 10; i++)
					{
						//上
						memset(&d3d9Line, 0x00, sizeof(D3D9_LINE));
						d3d9Line.usLineId = 1;
						//					sprintf(d3d9Line.strLineName, "L%d", i);
						d3d9Line.dwColor = RGB(0xFF, 0x00, 0xA0);
						d3d9Line.uiTotalNodes = 2;
						d3d9Line.pNodes[0].x = i * 10;
						d3d9Line.pNodes[0].y = 0;
						if (i % 5 == 0)
						{
							//sprintf(d3d9Line.strLineName, "%d", i);
							d3d9Line.pNodes[1].y = 20;
						}
						else
						{
							d3d9Line.pNodes[1].y = 10;
						}
						d3d9Line.pNodes[1].x = i * 10;
						D3D_AddLine(pThread->d3dHandle, &d3d9Line);

						//下
						memset(&d3d9Line, 0x00, sizeof(D3D9_LINE));
						d3d9Line.usLineId = 1;
						d3d9Line.dwColor = RGB(0xFF, 0xA0, 0x00);
						d3d9Line.uiTotalNodes = 2;
						d3d9Line.pNodes[0].x = i * 10;
						if (i % 5 == 0)
						{
							//sprintf(d3d9Line.strLineName, "%d", i);

							d3d9Line.pNodes[0].y = height - 20;
						}
						else
						{
							d3d9Line.pNodes[0].y = height - 10;
						}
						d3d9Line.pNodes[1].x = i * 10;
						d3d9Line.pNodes[1].y = height;
						D3D_AddLine(pThread->d3dHandle, &d3d9Line);

					}
#endif
				}
				if (NULL == pThread->d3dHandle)
				{
					_TRACE("DEVICE LOST...   times:%d\n", ((unsigned int)time(NULL) - deviceLostTime));
					deviceLostTime = (unsigned int)time(NULL);
					//如果d3d 初始化失败,则清空帧头信息,以便解码线程继续解码下一帧
					pThread->rtpTimestamp = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_sec * 1000 + pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_usec / 1000;
					memset(&pThread->yuvFrame[iDispalyYuvIdx].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
					_VS_BEGIN_TIME_PERIOD(1);
					__VS_Delay(1);
					_VS_END_TIME_PERIOD(1);
					continue;
				}
			}
		}

		int fps = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.fps;
		int iOneFrameUsec = 1000 / 30;	//normal
		if (fps > 0)	iOneFrameUsec = 1000 / fps;

		int iCache = 0;
		if (NULL != pThread->frameCache) iCache = pThread->frameCache;

		pThread->rtpTimestamp = pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_sec * 1000 + pThread->yuvFrame[iDispalyYuvIdx].frameinfo.timestamp_usec / 1000;


		D3D_OSD	osd[1024];
		memset(osd, 0, 1024);
		int osdLines = 0;
		int showOSD = pThread->showStatisticalInfo;

		if (NULL != pThread->hWnd && (IsWindow(pThread->hWnd)))
		{

			//统计信息:  编码格式 分辨率 帧率 帧类型  码流  缓存帧数
			char sztmp[128] = { 0, };
#if 0
			sprintf(sztmp, "%s[%d x %d]  FPS: %d[%s]    Bitrate: %.2fMbps   Cache: %d / %d",
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.codec == 0x1C ? "H264" : "MPEG4",
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.width,
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.height,
				fps,
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.type == 0x01 ? "I" : "P",
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.bitrate / 1024.0f,
				nQueueFrame, iCache);
#else
			sprintf(sztmp, "[%dx%d] fps[%d] Bitrate[%.2fMbps]Cache[%d(%d+%d) / %d]  AverageTime: %.2f  Delay: %d  totaltime:%d / %d dropframe:%d",
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.width,
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.height,
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.fps,
				pThread->yuvFrame[iDispalyYuvIdx].frameinfo.bitrate / 1024.0f,
				nQueueFrame, iQue1_DecodeQueue, iQue2_DisplayQueue, iCache,
				fDisplayTimes, iDelay, (int)fDisplayTimes + (iDelay > 0 ? iDelay : 0), iOneFrameUsec, iDropFrame);
#endif
			//D3D_OSD	osd;
			//memset(&osd, 0x00, sizeof(D3D_OSD));
			//MByteToWChar(sztmp, osd.string, sizeof(osd.string) / sizeof(osd.string[0]));
			//if (pThread->renderFormat == GDI_FORMAT_RGB24)
			//{
			//	SetRect(&osd.rect, 2, 2, (int)wcslen(osd.string) * 7, (int)(float)((height)*0.046f));//40);
			//}
			//else
			//{
			//	SetRect(&osd.rect, 2, 2, (int)wcslen(osd.string) * 200, (int)(float)((height)*0.085f));//40);
			//}
			//osd.color = RGB(0xff, 0xff, 0x00);
			//osd.shadowcolor = RGB(0x15, 0x15, 0x15);
			//osd.alpha = 180;

			if (pThread->showStatisticalInfo)
			{
				osdLines = 1;
				MByteToWChar(sztmp, osd[0].string, sizeof(osd[0].string) / sizeof(osd[0].string[0]));
				if (pThread->renderFormat == GDI_FORMAT_RGB24)
				{
					SetRect(&osd[0].rect, 2, 2, (int)wcslen(osd[0].string) * 7, (int)(float)((height)*0.046f));//40);
				}
				else
				{
					SetRect(&osd[0].rect, 2, 2, (int)wcslen(osd[0].string) * 200, (int)(float)((height)*0.3f));//40);
				}
				osd[0].color = RGB(0x00, 0xff, 0x00);
				//osd.shadowcolor = RGB(0x15,0x15,0x15);
				osd[0].shadowcolor = RGB(0x00, 0x00, 0x00);
				osd[0].alpha = 180;
			}
			if (pThread->showOSD)
			{
				showOSD = pThread->showOSD;
				std::string sOSD = pThread->osd.stOSD;
				while (!sOSD.empty())
				{
					char* subOSD = (char*)sOSD.c_str();
					int nOSDLen = sOSD.length();
					int sublen = 0;

					int nEofPos = sOSD.find("\r\n");

					if (nEofPos > 127)
					{
						subOSD[128] = 0;
						sublen = 128;
					}
					if (pThread->renderFormat == GDI_FORMAT_RGB24)
					{
						CopyRect(&osd[osdLines].rect, &pThread->osd.rect);
						osd[osdLines].rect.top = pThread->osd.rect.top + 40 * osdLines;
						osd[osdLines].rect.bottom = pThread->osd.rect.bottom + 40 * osdLines;
						if (nEofPos >= 0)
						{
							subOSD[nEofPos] = 0;
							sublen = nEofPos;
						}
					}
					else
					{
						CopyRect(&osd[osdLines].rect, &pThread->osd.rect);
						osd[osdLines].rect.top = pThread->osd.rect.top + pThread->osd.size*osdLines;
						osd[osdLines].rect.bottom = pThread->osd.rect.bottom + pThread->osd.size*osdLines;
						if (nEofPos >= 0)
						{
							subOSD[nEofPos] = 0;
							sublen = nEofPos;
						}
					}

					MByteToWChar(subOSD, osd[osdLines].string, sizeof(osd[osdLines].string) / sizeof(osd[osdLines].string[0]));

					osd[osdLines].color = pThread->osd.color;
					osd[osdLines].shadowcolor = pThread->osd.shadowcolor;
					osd[osdLines].alpha = pThread->osd.alpha;
					osdLines++;
					if (nEofPos >= 0)
					{
						sOSD = subOSD + nEofPos + 2;
					}
					else
					{
						sOSD = "";
					}
				}
			}
		}
		int ret = 0;

		memset(&lastFrameInfo, 0x00, sizeof(MEDIA_FRAME_INFO));
		memcpy(&lastFrameInfo, &pThread->yuvFrame[iDispalyYuvIdx].frameinfo, sizeof(MEDIA_FRAME_INFO));

		if (nQueueFrame > iCache * 2)	iDropFrame++;
		else							iDropFrame = 0;
		if (iDropFrame < 0x02)
		{
#if 1	//  [3/11/2016 Dingshuai]
			// 解码数据显示+回调
			//pRealtimePlayThread[iNvsIdx].pCallback = callback;
			if (pThread&&pThread->pCallback&&iDispalyYuvIdx >= 0)
			{
				// 硬件编码出来是NV12，外部为了显示方便应该转成I420 [12/6/2016 dingshuai]
#if 0
				if (pThread->renderFormat == D3D_FORMAT_NV12)
				{
					int i = 0;
					int nVideSize = width*height;
					int yuvDely = nVideSize / 4;
					int nHelfYUVSize = nVideSize / 2;
					// Write Cb  
					char* pTempUV = new char[nVideSize >> 1];
					char* pUV = pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf + nVideSize;
					memcpy(pTempUV, pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf + nVideSize, nVideSize >> 1);

					char tmpData;
					for (int idx = 0; idx < (nHelfYUVSize); idx += 2)
					{
						*(pUV + i) = pTempUV[idx];
						*(pUV + yuvDely + i) = pTempUV[idx + 1];
						i++;
					}
					if (pTempUV)
					{
						delete[] pTempUV;
					}
				}
#endif

				// 获取解码后的YUV/RGB数据的大小 [4/1/2016 Dingshuai]
				int nYuvFrameLen = pThread->yuvFrame[iDispalyYuvIdx].Yuvsize - 1;
				pThread->pCallback(pThread->channelId, (INT*)pThread->pUserPtr, SKEYE_SDK_DECODE_VIDEO_FLAG, pThread->renderFormat, pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, nYuvFrameLen,
					(SKEYE_FRAME_INFO*)(&pThread->yuvFrame[iDispalyYuvIdx].frameinfo));
			}
#endif

			if (NULL != pThread->hWnd && (IsWindow(pThread->hWnd)))
			{

				// 硬件解码将不再这里显示 [12/6/2016 dingshuai]
				//if (pThread->renderFormat != D3D_FORMAT_NV12 || (pThread->renderFormat == D3D_FORMAT_NV12&&!pThread->decoderObj[0].bInnerShow))
				if (pThread->decoderObj[0].bInnerShow)
				{
					if (pThread->decoderObj[0].nHardDecode != 2)
					{
						if (pThread->renderFormat == GDI_FORMAT_RGB24)
						{

#ifdef _WIN64
							GDI_DrawData(pThread->d3dHandle, pThread->hWnd, pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, width, height, &rcSrc, pThread->ShownToScale, RGB(0x3c, 0x3c, 0x3c), 0, osdLines, osd);
#else
							RGB_DrawData(pThread->d3dHandle, pThread->hWnd, pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, width, height, &rcSrc, pThread->ShownToScale, RGB(0x3c, 0x3c, 0x3c), 0, osdLines, osd);
#endif

							//D3D_RenderRGB24ByGDI(pThread->hWnd, pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, width, height, showOSD, &osd);
						}
						else if (NULL != pThread->d3dHandle)
						{
#ifdef _DEBUG0
							static FILE *fOutput = NULL;
							if (NULL == fOutput)
							{
								char sztmp[128] = { 0, };
								sprintf(sztmp, "C:\\test\\%dx%d.yv12", width, height);
								fOutput = fopen(sztmp, "wb");
							}
							if (NULL != fOutput)	fwrite(pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, 1, width * height * 3 / 2, fOutput);
#endif

							D3D_UpdateData(pThread->d3dHandle, 0, (unsigned char*)pThread->yuvFrame[iDispalyYuvIdx].pYuvBuf, width, height, &rcSrc, NULL, osdLines, osd);
							ret = D3D_Render(pThread->d3dHandle, pThread->hWnd, pThread->ShownToScale, &rcDst);
							if (ret < 0)
							{
								deviceLostTime = (unsigned int)time(NULL);
								pThread->resetD3d = true;						//需要重建D3D
							}
						}
					}
				}
			}
		}
		else
		{
			_TRACE("丢帧...%d\n", iDropFrame);
		}

		memset(&pThread->yuvFrame[iDispalyYuvIdx].frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));

		if (iInitTimestamp == 0x00)
		{
			iInitTimestamp = 0x01;
			_VS_BEGIN_TIME_PERIOD(1);
			__VS_Delay(50);
			QueryPerformanceCounter(&pThread->lastRenderTime);
			_VS_END_TIME_PERIOD(1);
				}
		else
		{
			unsigned int iInterval = 0;
			_LARGE_INTEGER	nowTime;


#ifdef _DEBUG1
			//static int nRandTally = 0;
			//if (nRandTally > 20)
			//{
			//Sleep(50);
			//if (nRandTally > 30)	nRandTally = 0;
			//}
			//nRandTally ++;


			Sleep(30);
#endif


			_VS_BEGIN_TIME_PERIOD(1);
			QueryPerformanceCounter(&nowTime);
			_VS_END_TIME_PERIOD(1);
			if (pThread->cpuFreq.QuadPart < 1)	pThread->cpuFreq.QuadPart = 1;
			LONGLONG lInterval = (LONGLONG)(((nowTime.QuadPart - pThread->lastRenderTime.QuadPart) / (double)pThread->cpuFreq.QuadPart * (double)1000));
			iInterval = (int)lInterval;

			iDelay = iOneFrameUsec - iInterval;

			if (iDelay<1)	iDelay = 0;
			else if (iDelay>1500)	iDelay = 1500;
			if (nQueueFrame<iCache && fps>0)
			{
				int ii = ((iOneFrameUsec * (iCache - nQueueFrame)) / fps);
				iDelay += ii;
			}
			else if (nQueueFrame>iCache && fps>0)
			{
				int ii = ((iOneFrameUsec * (nQueueFrame - iCache)) / fps);
				iDelay -= ii;
			}

			//#ifdef _DEBUG
#if 1
			unsigned int uiCurrTime = (unsigned int)time(NULL);
			if (uiLastTotalTime == 0x00)		uiLastTotalTime = uiCurrTime;

			if (uiLastTotalTime != uiCurrTime)
			{
				fDisplayTimes = (float)nDisplayTotalTimes / (float)displayFrameNum;
				//fDisplayTimes = (float)nDisplayTotalTimes / iOneFrameUsec;
				uiLastTotalTime = uiCurrTime;
				nDisplayTotalTimes = iInterval;
				displayFrameNum = 1;
			}
			else
			{
				nDisplayTotalTimes += iInterval;
				displayFrameNum++;
			}
#endif

			_VS_BEGIN_TIME_PERIOD(1);
			//_TRACE("[ch%d]共用时: %d\t显示耗时:%d\t延时:%d\t缓存帧数:%d\t当前帧大小:%d  OneFrameUsec:%d\n", pThread->renderCh, iInterval+iDelay, iInterval, iDelay, nQueueFrame, frame_size, iOneFrameUsec);
			if (iDelay>0 && iDelay<1500 && iCache>0 && iCache * 2>nQueueFrame && iDropFrame == 0)
			{
				__VS_Delay(iDelay);
			}

			QueryPerformanceCounter(&pThread->lastRenderTime);
			_VS_END_TIME_PERIOD(1);
		}
	}

	if (pThread->renderFormat == GDI_FORMAT_RGB24)
	{
#ifdef _WIN64
		GDI_DeinitDraw(&pThread->d3dHandle);
#else
		RGB_DeinitDraw(&pThread->d3dHandle);
#endif
	}
	else
	{
		D3D_Release(&pThread->d3dHandle);
	}
	pThread->rtpTimestamp = 0;

	pThread->displayThread.flag = 0x00;

#ifdef _DEBUG
	_TRACE("显示线程[%d]已退出. ThreadId:%d ..\n", pThread->channelId, GetCurrentThreadId());
#endif

	return 0;
}

int CALLBACK __RTSPSourceCallBack( int _channelId, void *_channelPtr, int _frameType, char *pBuf, SKEYE_FRAME_INFO* _frameInfo)
{
	PLAY_THREAD_OBJ	*pPlayThread = (PLAY_THREAD_OBJ *)_channelPtr;

	if (NULL == pPlayThread)			return -1;

	if (NULL == pChannelManager)	return -1;

	if (NULL != _frameInfo)
	{
		if (_frameInfo->height==3008)			_frameInfo->height=3000;
		else if (_frameInfo->height==1088)		_frameInfo->height=1080;
		else if (_frameInfo->height==544)		_frameInfo->height=540;
	}

	pChannelManager->ProcessData(_channelId, _frameType, pBuf, _frameInfo);

	return 0;
}

int	CChannelManager::ProcessData(int _chid, int mediatype, char *pbuf, SKEYE_FRAME_INFO *frameinfo)
{
	if (NULL == pRealtimePlayThread)			return 0;
	if (_chid<0)
	{
		return -1;
	}

	MediaSourceCallBack pMediaCallback = (MediaSourceCallBack )pRealtimePlayThread[_chid].pCallback;
	if (NULL != pMediaCallback && (mediatype == SKEYE_SDK_VIDEO_FRAME_FLAG || mediatype == SKEYE_SDK_AUDIO_FRAME_FLAG /*|| mediatype == SKEYE_SDK_MEDIA_INFO_FLAG*/))
	{
		int nFrameLen = 0;
		int nDataType = -1;
		if (frameinfo)
		{
			nFrameLen = frameinfo->length;
			nDataType = frameinfo->codec;
		}
		pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, nDataType, pbuf, nFrameLen, frameinfo);
	}
	if (NULL != pMediaCallback && mediatype == SKEYE_SDK_MEDIA_INFO_FLAG)
	{
		int nFrameLen = 0;
		int nDataType = -1;
		if (frameinfo)
		{
			nFrameLen = frameinfo->length;
			nDataType = frameinfo->codec;
		}
		pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, nDataType, pbuf, nFrameLen, frameinfo);
	}

	if (mediatype == SKEYE_SDK_VIDEO_FRAME_FLAG)
	{
		if (frameinfo)
		{
			if (frameinfo->codec == SKEYE_SDK_VIDEO_CODEC_H265)
			{
				//if (frameinfo->width == 0 )
				//{
				//	frameinfo->width = 5904;
				//	frameinfo->height = 896;
				//}
				int nal_type = (pbuf[4] >> 1) & 0x3F;
				if ((nal_type >= 16 && nal_type <= 21) || nal_type == 32)//P 或者 I
				{
					frameinfo->type = 0x01;
				}
				else if (nal_type >= 0 && nal_type <= 9)
				{
					frameinfo->type = 0x02;
				}
			}
			else if (frameinfo->codec == SKEYE_SDK_VIDEO_CODEC_H264)
			{
				//_TRACE("Buffer...[%02x][%02x][%02x][%02x][%02x][%02x]\n", pbuf[0], pbuf[1], pbuf[2], pbuf[3], pbuf[4], pbuf[5] );
				int nal_type = pbuf[4] & 0x1F;
				if (nal_type == 0x07 || nal_type == 0x05)//P 或者 I
				{
					frameinfo->type = 0x01;
				}
				else if (nal_type == 0x01)
				{
					frameinfo->type = 0x02;
				}
			}
		}
		if ( (NULL == pRealtimePlayThread[_chid].pAVQueue) && (frameinfo->type==SKEYE_SDK_VIDEO_FRAME_I/*Key frame*/) )
		{
			pRealtimePlayThread[_chid].pAVQueue = new SS_QUEUE_OBJ_T();
			if (NULL != pRealtimePlayThread[_chid].pAVQueue)
			{
				memset(pRealtimePlayThread[_chid].pAVQueue, 0x00, sizeof(SS_QUEUE_OBJ_T));
				SSQ_Init(pRealtimePlayThread[_chid].pAVQueue, 0x00, _chid, TEXT(""), MAX_AVQUEUE_SIZE, 2, 0x01);
				SSQ_Clear(pRealtimePlayThread[_chid].pAVQueue);
				pRealtimePlayThread[_chid].initQueue = 0x01;
			}

			pRealtimePlayThread[_chid].dwLosspacketTime=0;
			pRealtimePlayThread[_chid].dwDisconnectTime=0;
		}
		if (NULL != pRealtimePlayThread[_chid].pAVQueue)
		{
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_VIDEO, (MEDIA_FRAME_INFO*)frameinfo, pbuf);
		}
	}
	else if (mediatype == SKEYE_SDK_AUDIO_FRAME_FLAG)
	{

		if (NULL != pRealtimePlayThread[_chid].pAVQueue)
		{
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_AUDIO, (MEDIA_FRAME_INFO*)frameinfo, pbuf);
		}
	}
// 	else if (mediatype == SKEYE_SDK_EVENT_FRAME_FLAG)
// 	{
// 		if (NULL == pbuf && NULL == frameinfo)
// 		{
// 			_TRACE("[ch%d]连接中...\n", _chid);
// 
// 			MEDIA_FRAME_INFO	frameinfo;
// 			memset(&frameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
// 			frameinfo.length = 1;
// 			frameinfo.type   = 0xFF;
// 			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)&frameinfo, "1");
// 		}
// 		else if (NULL!=frameinfo && frameinfo->type==0xF1)
// 		{
// 			_TRACE("[ch%d]掉包[%.2f]...\n", _chid, frameinfo->losspacket);
// 
// 			frameinfo->length = 1;
// 			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)frameinfo, "1");
// 		}
// 	}
	else if (mediatype == SKEYE_SDK_EVENT_FRAME_FLAG)//回调连接状态事件
	{
		// SkeyeRTSPClient开始进行连接，建立SkeyeRTSPClient连接线程
		char*  fRTSPURL = pRealtimePlayThread[_chid].url;
		char sErrorString[512];
		if (NULL == pbuf && NULL == frameinfo)
		{
			sprintf(sErrorString, "Connecting:	%s ...\n", fRTSPURL);

			MEDIA_FRAME_INFO	tmpFrameinfo;
			memset(&tmpFrameinfo, 0x00, sizeof(MEDIA_FRAME_INFO));
			tmpFrameinfo.length = 1;
			tmpFrameinfo.type   = 0xFF;
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)&tmpFrameinfo, sErrorString);
			if (pMediaCallback)
				pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, 0, sErrorString, strlen(sErrorString), (SKEYE_FRAME_INFO*)&tmpFrameinfo);
		}
		else if (NULL!=frameinfo && frameinfo->type==0xF1)
		{
#if 0
			sprintf(sErrorString, "Error:	 %s：	[ch%d]掉包[%.2f]...\n",  fRTSPURL, _chid, frameinfo->losspacket);

			frameinfo->length = 1;
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)frameinfo, sErrorString);
			if (pMediaCallback)
				pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, 0xF1, sErrorString, strlen(sErrorString),  frameinfo);
#endif
		}

		// SkeyeRTSPClient RTSPClient连接错误，错误码通过SkeyeRTSP_GetErrCode()接口获取，比如404
		else if (NULL != frameinfo && frameinfo->codec == SKEYE_SDK_EVENT_CODEC_ERROR)
		{
			sprintf(sErrorString, "Error:	  %s： SkeyeRTSP_GetErrCode：%d :%s ...\n", fRTSPURL, SkeyeRTSP_GetErrCode(pRealtimePlayThread[_chid].nvsHandle), pbuf?pbuf:"null" );
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)frameinfo, sErrorString);
			if (pMediaCallback)
				pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, SKEYE_SDK_EVENT_CODEC_ERROR, sErrorString, strlen(sErrorString),  frameinfo);
		}

		// SkeyeRTSPClient连接线程退出，此时上层应该停止相关调用，复位连接按钮等状态
		else if (NULL != frameinfo && frameinfo->codec == SKEYE_SDK_EVENT_CODEC_EXIT)
		{
			sprintf(sErrorString, "Exit:	%s,   Error:%d ...\n", fRTSPURL, SkeyeRTSP_GetErrCode(pRealtimePlayThread[_chid].nvsHandle));
			SSQ_AddData(pRealtimePlayThread[_chid].pAVQueue, _chid, MEDIA_TYPE_EVENT, (MEDIA_FRAME_INFO*)frameinfo, sErrorString);
			if (pMediaCallback)
				pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, SKEYE_SDK_EVENT_CODEC_EXIT, sErrorString, strlen(sErrorString), frameinfo);
		}
		else
		{
			int nFrameLen = 0;
			int nDataType = -1;
			if (frameinfo)
			{
				nFrameLen = frameinfo->length;
				nDataType = frameinfo->codec;
			}
			pMediaCallback(_chid, (int*)pRealtimePlayThread[_chid].pUserPtr, mediatype, nDataType, pbuf, nFrameLen, frameinfo);
		}
	}
	return 0;
}

int	CChannelManager::SetManuRecordPath(int channelId, const char* recordPath)
{
	if (NULL == pRealtimePlayThread)			return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (recordPath)
	{
		int nPathLen = strlen(recordPath);
		if (nPathLen<=0)
		{
			return -1;
		}
		if (nPathLen>MAX_URL_LENGTH-1)
		{
			nPathLen = MAX_URL_LENGTH-1;
		}
		memcpy(pRealtimePlayThread[iNvsIdx].manuRecordingPath, recordPath, nPathLen+1);
		int nRecordPathLen = strlen(pRealtimePlayThread[iNvsIdx].manuRecordingPath);
		if (nRecordPathLen == 0 || (pRealtimePlayThread[iNvsIdx].manuRecordingPath[nRecordPathLen - 1] != '/' && pRealtimePlayThread[iNvsIdx].manuRecordingPath[nRecordPathLen - 1] != '\\'))
		{
			pRealtimePlayThread[iNvsIdx].manuRecordingPath[nRecordPathLen] = '\\';
		}
	}
	return 1;
}

int	CChannelManager::SetManuPicShotPath(int channelId, const char* shotPath)
{
	if (NULL == pRealtimePlayThread)			return -1;
	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;
	if (shotPath)
	{
		int nPathLen = strlen(shotPath);
		if (nPathLen<=0)
		{
			return -1;
		}
		if (nPathLen>MAX_URL_LENGTH-1)
		{
			nPathLen = MAX_URL_LENGTH-1;
		}

		memcpy(pRealtimePlayThread[iNvsIdx].strScreenCapturePath, shotPath, nPathLen+1);
		int nRecordPathLen = strlen(pRealtimePlayThread[iNvsIdx].strScreenCapturePath);
		if (nRecordPathLen == 0 || (pRealtimePlayThread[iNvsIdx].strScreenCapturePath[nRecordPathLen - 1] != '/' && pRealtimePlayThread[iNvsIdx].strScreenCapturePath[nRecordPathLen - 1] != '\\'))
		{
			pRealtimePlayThread[iNvsIdx].strScreenCapturePath[nRecordPathLen] = '\\';
		}
	}
	return 1;
}

int	CChannelManager::StartManuPicShot(int channelId)	
{
	//pThread->manuScreenshot
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	//if (NULL == pRealtimePlayThread[iNvsIdx].m_pSaveImage)
	{
		pRealtimePlayThread[iNvsIdx].manuScreenshot = 0x01;
	}
	return 1;
}

int	CChannelManager::StopManuPicShot(int channelId)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	pRealtimePlayThread[iNvsIdx].manuScreenshot = 0x00;
	return 1;
}


int	CChannelManager::StartManuRecording(int channelId)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (NULL == pRealtimePlayThread[iNvsIdx].m_pMP4Writer)
	{
		pRealtimePlayThread[iNvsIdx].manuRecording = 0x01;
	}

	return 1;
}
int	CChannelManager::StopManuRecording(int channelId)
{
	if (NULL == pRealtimePlayThread)			return -1;

	int iNvsIdx = channelId - CHANNEL_ID_GAIN;
	if (iNvsIdx < 0 || iNvsIdx>= MAX_CHANNEL_NUM)	return -1;

	if (pRealtimePlayThread[iNvsIdx].manuRecording == 0x01)
	{
		pRealtimePlayThread[iNvsIdx].manuRecording = 0x00;
	}

	return 1;
}

