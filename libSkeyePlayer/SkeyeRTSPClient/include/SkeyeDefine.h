/*
	Copyright (c) 2018-2022 OpenSKEYE.CN.  All rights reserved.
	Gitee: https://gitee.com/visual-opening
	WEChat: OpenSKEYE
	Website: http://www.openskeye.cn/
*/

#ifndef _Skeye_Types_H
#define _Skeye_Types_H

#ifdef _WIN32
#define Skeye_API  __declspec(dllexport)
#define Skeye_APICALL  __stdcall
#define WIN32_LEAN_AND_MEAN
#else
#define Skeye_API
#define Skeye_APICALL 
#endif

#define Skeye_Handle void*

typedef int						Skeye_I32;
typedef unsigned char           Skeye_U8;
typedef unsigned char           Skeye_UChar;
typedef unsigned short          Skeye_U16;
typedef unsigned int            Skeye_U32;
typedef unsigned char			Skeye_Bool;

enum
{
    Skeye_NoErr						= 0,
    Skeye_RequestFailed				= -1,
    Skeye_Unimplemented				= -2,
    Skeye_RequestArrived				= -3,
    Skeye_OutOfState					= -4,
    Skeye_NotAModule					= -5,
    Skeye_WrongVersion				= -6,
    Skeye_IllegalService				= -7,
    Skeye_BadIndex					= -8,
    Skeye_ValueNotFound				= -9,
    Skeye_BadArgument				= -10,
    Skeye_ReadOnly					= -11,
	Skeye_NotPreemptiveSafe			= -12,
    Skeye_NotEnoughSpace				= -13,
    Skeye_WouldBlock					= -14,
    Skeye_NotConnected				= -15,
    Skeye_FileNotFound				= -16,
    Skeye_NoMoreData					= -17,
    Skeye_AttrDoesntExist			= -18,
    Skeye_AttrNameExists				= -19,
    Skeye_InstanceAttrsNotAllowed	= -20,
	Skeye_InvalidSocket				= -21,
	Skeye_MallocError				= -22,
	Skeye_ConnectError				= -23,
	Skeye_SendError					= -24
};
typedef int Skeye_Error;

typedef enum __SKEYE_ACTIVATE_ERR_CODE_ENUM
{
	SKEYE_ACTIVATE_INVALID_KEY		=		-1,			/* 无效Key */
	SKEYE_ACTIVATE_TIME_ERR			=		-2,			/* 时间错误 */
	SKEYE_ACTIVATE_PROCESS_NAME_LEN_ERR	=	-3,			/* 进程名称长度不匹配 */
	SKEYE_ACTIVATE_PROCESS_NAME_ERR	=		-4,			/* 进程名称不匹配 */
	SKEYE_ACTIVATE_VALIDITY_PERIOD_ERR=		-5,			/* 有效期校验不一致 */
	SKEYE_ACTIVATE_PLATFORM_ERR		=		-6,			/* 平台不匹配 */
	SKEYE_ACTIVATE_COMPANY_ID_LEN_ERR=		-7,			/* 授权使用商不匹配 */
	SKEYE_ACTIVATE_SUCCESS			=		0,		/* 激活成功 */

}SKEYE_ACTIVATE_ERR_CODE_ENUM;

/* 视频编码 */
#define SKEYE_SDK_VIDEO_CODEC_H264	0x1C		/* H264  */
#define SKEYE_SDK_VIDEO_CODEC_H265	0xAE	  /* H265 */
#define	SKEYE_SDK_VIDEO_CODEC_MJPEG	0x08		/* MJPEG */
#define	SKEYE_SDK_VIDEO_CODEC_MPEG4	0x0D		/* MPEG4 */

/* 音频编码 */
#define SKEYE_SDK_AUDIO_CODEC_PCM	0x10000		/* pcm */
#define SKEYE_SDK_AUDIO_CODEC_AAC	0x15002		/* AAC */
#define SKEYE_SDK_AUDIO_CODEC_G711U	0x10006		/* G711 ulaw*/
#define SKEYE_SDK_AUDIO_CODEC_G711A	0x10007		/* G711 alaw*/
#define SKEYE_SDK_AUDIO_CODEC_G726	0x1100B		/* G726 */


#define SKEYE_SDK_EVENT_CODEC_ERROR	0x63657272	/* ERROR */
#define SKEYE_SDK_EVENT_CODEC_EXIT	0x65786974	/* EXIT */

/* 音视频帧标识 */
#define SKEYE_SDK_VIDEO_FRAME_FLAG	0x00000001		/* 视频帧标志 */
#define SKEYE_SDK_AUDIO_FRAME_FLAG	0x00000002		/* 音频帧标志 */
#define SKEYE_SDK_EVENT_FRAME_FLAG	0x00000004		/* 事件帧标志 */
#define SKEYE_SDK_RTP_FRAME_FLAG	0x00000008		/* RTP帧标志 */
#define SKEYE_SDK_SDP_FRAME_FLAG	0x00000010		/* SDP帧标志 */
#define SKEYE_SDK_MEDIA_INFO_FLAG	0x00000020		/* 媒体类型标志*/
#define SKEYE_SDK_SNAP_FRAME_FLAG	0x00000040		/* 图片标志*/
#define SKEYE_SDK_DECODE_VIDEO_FLAG 0x00000080		/* 解码视频类型标志*/
#define SKEYE_SDK_DECODE_AUDO_FLAG	0x00000100		/* 解码音频类型标志*/
#define SKEYE_SDK_RESET_VIDEO_RENDER 0x00000101		/*重建D3D标志*/	


/* 视频关键字标识 */
#define SKEYE_SDK_VIDEO_FRAME_I		0x01		/* I帧 */
#define SKEYE_SDK_VIDEO_FRAME_P		0x02		/* P帧 */
#define SKEYE_SDK_VIDEO_FRAME_B		0x03		/* B帧 */
#define SKEYE_SDK_VIDEO_FRAME_J		0x04		/* JPEG */

/* 连接类型 */
typedef enum __SKEYE_RTP_CONNECT_TYPE
{
	SKEYE_RTP_OVER_TCP	=	0x01,		/* RTP Over TCP */
	SKEYE_RTP_OVER_UDP,					/* RTP Over UDP */
	SKEYE_RTP_OVER_MULTICAST			/* RTP Over MULTICAST */
}SKEYE_RTP_CONNECT_TYPE;

typedef struct __SKEYE_AV_Frame
{
	Skeye_U32    u32AVFrameFlag;	/* 帧标志  视频 or 音频 */
	Skeye_U32    u32AVFrameLen;		/* 帧的长度 */
	Skeye_U32    u32AVFrameType;	/* 视频的类型，I帧或P帧； 音频帧编码类型：AAC G711A G711U G726 */
	Skeye_U8     *pBuffer;			/* 数据 */
	Skeye_U32	u32TimestampSec;	/* 时间戳(秒)*/
	Skeye_U32	u32TimestampUsec;	/* 时间戳(微秒) */
} SKEYE_AV_Frame;

/* 媒体信息 */
typedef struct __SKEYE_MEDIA_INFO_T
{
	Skeye_U32 u32VideoCodec;			/* 视频编码类型 */
	Skeye_U32 u32VideoFps;				/* 视频帧率 */

	Skeye_U32 u32AudioCodec;			/* 音频编码类型 */
	Skeye_U32 u32AudioSamplerate;		/* 音频采样率 */
	Skeye_U32 u32AudioChannel;			/* 音频通道数 */
	Skeye_U32 u32AudioBitsPerSample;	/* 音频采样精度 */

	Skeye_U32 u32VpsLength;
	Skeye_U32 u32SpsLength;
	Skeye_U32 u32PpsLength;
	Skeye_U32 u32SeiLength;
	Skeye_U8	 u8Vps[256];
	Skeye_U8	 u8Sps[256];
	Skeye_U8	 u8Pps[128];
	Skeye_U8	 u8Sei[128];
}SKEYE_MEDIA_INFO_T;

/* 帧信息 */
typedef struct 
{
	unsigned int	codec;				/* 音视频格式 */

	unsigned int	type;				/* 视频帧类型 */
	unsigned char	fps;				/* 视频帧率 */
	unsigned short	width;				/* 视频宽 */
	unsigned short  height;				/* 视频高 */

	unsigned int	reserved1;			/* 保留参数1 */
	unsigned int	reserved2;			/* 保留参数2 */

	unsigned int	sample_rate;		/* 音频采样率 */
	unsigned int	channels;			/* 音频声道数 */
	unsigned int	bits_per_sample;	/* 音频采样精度 */

	unsigned int	length;				/* 音视频帧大小 */
	unsigned int    timestamp_usec;		/* 时间戳,微妙 */
	unsigned int	timestamp_sec;		/* 时间戳 秒 */
	
	float			bitrate;			/* 比特率 */
	float			losspacket;			/* 丢包率 */
}SKEYE_FRAME_INFO;

#endif
