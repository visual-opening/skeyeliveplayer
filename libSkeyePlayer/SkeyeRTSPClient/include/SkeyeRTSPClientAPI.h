/*
Copyright (c) 2018-2023 OpenSKEYE.CN.  All rights reserved.
Github: https://gitee.com/visual-opening/skeyeplayer
WEChat: OpenSKEYE
Website: http://www.OpenSKEYE.CN
Author: Sword@OpenSKEYE.CN
*/
#ifndef _Skeye_RTSPClient_API_H
#define _Skeye_RTSPClient_API_H

#include "SkeyeDefine.h"

#define	RTSP_PROG_NAME	"SkeyeRTSPClient v2.0.22.0222"

/*
	_channelId:		通道号,暂时不用
	_channelPtr:	通道对应对象,暂时不用
	_frameType:		SKEYE_SDK_VIDEO_FRAME_FLAG/SKEYE_SDK_AUDIO_FRAME_FLAG/SKEYE_SDK_EVENT_FRAME_FLAG/...	
	_pBuf:			回调的数据部分，具体用法看Demo
	_frameInfo:		帧结构数据
*/
typedef int (Skeye_APICALL *RTSPSourceCallBack)( int _channelId, void *_channelPtr, int _frameType, char *pBuf, SKEYE_FRAME_INFO* _frameInfo);

#ifdef __cplusplus
extern "C"
{
#endif
	/* 获取最后一次错误的错误码 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_GetErrCode(Skeye_Handle handle);

	/* 激活 */
#ifdef ANDROID
	Skeye_API int Skeye_APICALL SkeyeRTSP_Activate(char *license, char* userPtr);
#else
	Skeye_API int Skeye_APICALL SkeyeRTSP_Activate(char *license);
#endif

	/* 创建RTSPClient句柄  返回0表示成功，返回非0表示失败 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_Init(Skeye_Handle *handle);

	/* 释放RTSPClient 参数为RTSPClient句柄 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_Deinit(Skeye_Handle *handle);

	/* 设置数据回调 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_SetCallback(Skeye_Handle handle, RTSPSourceCallBack _callback);

	/* 打开网络流 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_OpenStream(Skeye_Handle handle, int _channelid, char *_url, SKEYE_RTP_CONNECT_TYPE _connType, unsigned int _mediaType, 
													char *_username, char *_password, void *userPtr, 
													int _reconn/*1000表示长连接,即如果网络断开自动重连, 其它值为连接次数*/, 
													int outRtpPacket/*默认为0,即回调输出完整的帧, 如果为1,则输出RTP包*/, 
													int heartbeatType/*0x00:不发送心跳 0x01:OPTIONS 0x02:GET_PARAMETER*/, 
													int _verbosity/*日志打印输出等级，0表示不输出*/,
													char *startTime, char *endTime, float fScale);
	
	/* 关闭网络流 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_CloseStream(Skeye_Handle handle);

	/*  设置播放速度 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_SetStreamSpeed(Skeye_Handle handle, float fSpeed);
	
	/* 跳转播放时间 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_SeekStream(Skeye_Handle handle, const char *playTime);

	/* 暂停 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_PauseStream(Skeye_Handle handle);

	/* 恢复播放 */
	Skeye_API int Skeye_APICALL SkeyeRTSP_ResumeStream(Skeye_Handle handle);

#ifdef __cplusplus
}
#endif

#endif
