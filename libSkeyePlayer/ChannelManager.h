/*
	Copyright (c) 2018-2023 OpenSKEYE.CN.  All rights reserved.
	Github: https://gitee.com/visual-opening/skeyeplayer
	WEChat: OpenSKEYE
	Website: http://www.OpenSKEYE.CN
	Author: Sword@OpenSKEYE.CN
*/
#pragma once

#include "libSkeyePlayerAPI.h"
#include "SkeyeRTSPClient\include\SkeyeRTSPClientAPI.h"
#include "FFCodec\include\FFCodecAPI.h"
#include "SoundPlayer.h"
#include "ssqueue.h"

#include "IntelHardCodec/include/IntelHardCodec_Interface.h"
#include "libSkeyeAACEncoder/include/SkeyeAACEncoderAPI.h"
//#include "SaveJPGDll/include/SaveJpg_Interface.h"
//gpac write mp4 file
#include "SkeyeMP4Writer/SkeyeMP4Writer.h"

#ifdef _WIN64
#include ".\D3DRender\include\x64\D3DRenderAPI.h"
#pragma comment(lib, "../bin_x64/D3DRender.lib")

#pragma comment(lib, "SkeyeRTSPClient/lib/x64/libSkeyeRTSPClient.lib")
#pragma comment(lib, "FFCodec/lib/x64/FFCodec.lib")
#pragma comment(lib, "IntelHardCodec/lib/x64/IntelHardCodec.lib")
#pragma comment(lib, "libSkeyeAACEncoder/lib/x64/libSkeyeAACEncoder.lib")
// 增加MP4box和SaveJPG库的支持 [9/20/2016 dingshuai]
//SKEYELIVE Client
#include "skeyeliveclient\liclude\skeyeliveplayer.h"
#pragma comment(lib, "skeyeliveclient/lib/x64/libskeyeliveplayer.lib")

#else
#include "D3DRender\include\x86\D3DRenderAPI.h"

#pragma comment(lib, "SkeyeRTSPClient/lib/x86/libSkeyeRTSPClient.lib")
#pragma comment(lib, "FFCodec/lib/x86/FFCodec.lib")
#pragma comment(lib, "D3DRender/lib/x86/D3DRender.lib")
#pragma comment(lib, "IntelHardCodec/lib/x86/IntelHardCodec.lib")
#pragma comment(lib, "libSkeyeAACEncoder/lib/x86/libSkeyeAACEncoder.lib")
// 增加MP4box和SaveJPG库的支持 [9/20/2016 dingshuai]
// JPGSave
//#pragma comment(lib, "SaveJPGDll/lib/x86/SaveJpgDll.lib")
#endif

//NvDecoder支持
#include "NvDecoder\NvDecoderAPI.h"
//#ifdef _WIN64
//#pragma comment(lib, "NvDecoder/x64/NvDecoder.lib")
//#else
//#pragma comment(lib, "NvDecoder/NvDecoder.lib")
//#endif

//libyuv库支持
#include "libyuv.h"

using namespace skeye_live;

#define		MAX_CHANNEL_NUM		64		//可以解码显示的最大通道数
#define		MAX_DECODER_NUM		5		//一个播放线程中最大解码器个数
#define		MAX_YUV_FRAME_NUM	1		//解码后的最大YUV帧数
#define		MAX_CACHE_FRAME		30		//最大帧缓存,超过该值将只播放I帧
#define		MAX_AVQUEUE_SIZE	(1024*1024)	//队列大小
//#define		MAX_AVQUEUE_SIZE	(1920*1080*2)	//队列大小
#define     MAX_URL_LENGTH		512

#ifdef _WIN64
#define FFMPEG_VIDEO_CODEC_H265 175
#else
#define FFMPEG_VIDEO_CODEC_H265 174
#endif
typedef struct __CODEC_T
{
	//Video Codec
	unsigned int	vidCodec;
	int				width;
	int				height;
	int				fps;
	float			bitrate;

	//Audio Codec
	unsigned int	audCodec;
	int				samplerate;
	int				channels;
	int				bits_per_sample;
}CODEC_T;
typedef struct __DECODER_OBJ
{
	bool bInnerShow;
	int	 nHardDecode;		//硬件解码标识 0=软解码 1=NvDecoder 2=IntelDecoder
	CODEC_T			codec;
	FFC_HANDLE		ffDecoder;
	NVDECODER_HANDLE	pNvDecoder;
	LPIntelHardDecoder pIntelDecoder;
	int				yuv_size;
}DECODER_OBJ;


typedef struct __THREAD_OBJ
{
	int			flag;
	HANDLE		hThread;
}THREAD_OBJ;

typedef struct _YUV_FRAME_INFO			//YUV信息
{
	MEDIA_FRAME_INFO	frameinfo;
	char	*pYuvBuf;
	int		Yuvsize;
}YUV_FRAME_INFO;

typedef struct __PLAY_THREAD_OBJ
{
	THREAD_OBJ		decodeThread;		//解码线程
	THREAD_OBJ		displayThread;		//显示线程
	Skeye_Handle	nvsHandle;
	int				nLiveType;//0-RTSPClient 1-SkeyeLiveClient

	HWND			hWnd;				//显示视频的窗口句柄
	int				channelId;			//通道号
	int				showStatisticalInfo;//显示统计信息
	int				showOSD;//显示OSD信息
	//OSD自定义叠加
	SKEYE_PALYER_OSD osd;
	int				frameCache;		//帧缓存(用于调整流畅度),由上层应用设置
	int				initQueue;		//初始化队列标识
	SS_QUEUE_OBJ_T	*pAVQueue;		//接收rtsp的帧队列
	int				frameQueue;		//队列中的帧数
	int				findKeyframe;	//是否需要查找关键帧标识
	int				decodeYuvIdx;
	DWORD			dwLosspacketTime;	//丢包时间
	DWORD			dwDisconnectTime;	//断线时间
	DECODER_OBJ		decoderObj[MAX_DECODER_NUM];
	D3D_HANDLE		d3dHandle;		//显示句柄
	D3D_SUPPORT_FORMAT	renderFormat;	//显示格式
	int				ShownToScale;		//按比例显示
	int				decodeKeyFrameOnly;	//仅解码显示关键帧
	unsigned int	rtpTimestamp;
	LARGE_INTEGER	cpuFreq;		//cpu频率
	_LARGE_INTEGER	lastRenderTime;	//最后显示时间
	int				yuvFrameNo;		//当前显示的yuv帧号
	YUV_FRAME_INFO	yuvFrame[MAX_YUV_FRAME_NUM];
	int				yuvFramePlannelSize;
	int				yuvFrameWidth;
	int				yuvFrameHeight;
	CRITICAL_SECTION	crit;
	bool			resetD3d;		//是否需要重建d3dRender
	RECT			rcSrcRender;
	D3D9_LINE		d3d9Line;	
	char			url[MAX_URL_LENGTH];//播放的URL路径
	char			manuRecordingPath[MAX_URL_LENGTH];//录像存放路径
	char			strScreenCapturePath[MAX_URL_LENGTH];//抓图存放路径
	//char			manuRecordingFile[MAX_PATH];
	int				manuRecording;	//=1 开启录制
	int				manuScreenshot;//=1 开启抓图
	//存储JPG库
	//LPSaveJpg	   m_pSaveImage;
	//MP4Box Writer
	SkeyeMP4Writer* m_pMP4Writer;
	int				vidFrameNum;
	MediaSourceCallBack pCallback;
	void			*pUserPtr;
}PLAY_THREAD_OBJ;


//音频播放线程
typedef struct __AUDIO_PLAY_THREAD_OBJ
{
	int				channelId;		//当前播放通道号

	unsigned int	samplerate;	//采样率
	unsigned int	audiochannels;	//声道
	unsigned int	bitpersample;

	//CWaveOut		*pWaveOut;
	CSoundPlayer	*pSoundPlayer;
}AUDIO_PLAY_THREAD_OBJ;

//抓图线程结构体
typedef struct tagPhotoShotThreadInfo
{
	unsigned char* pYuvBuf;
	int width;
	int height;
	char strPath[MAX_URL_LENGTH];
	int renderFormat;
}PhotoShotThreadInfo;


class CChannelManager
{
public:
	CChannelManager(void);
	virtual ~CChannelManager(void);

	int		Initial();

	//OpenStream 返回一个可用的通道ID
	int		OpenStream(const char *url, HWND hWnd, RENDER_FORMAT renderFormat, int _rtpovertcp, const char *username, const char *password, MediaSourceCallBack callback=NULL, void *userPtr=NULL, char* startTime = NULL, char* endTime=NULL, float fScale = 1.0f, int decodeType= 1, bool bInnerShow=true);
	void		CloseStream(int channelId);
	////////////////////////////////////////////////////////////////////////////////////
	// RTSP回放调用接口
	/*  设置播放速度（播放直播流时无效） */
	int SetStreamSpeed(int channelId, float fSpeed);
	/* 跳转播放时间 */
	int SeekStream(int channelId, const char *playTime);
	/* 暂停 */
	int PauseStream(int channelId);
	/* 恢复播放 */
	int ResumeStream(int channelId);
	/////////////////////////////////////////////////////////////////////////////////////

	int		ShowStatisticalInfo(int channelId, int _show);
	int		ShowOSD(int channelId, int _show, SKEYE_PALYER_OSD osd);
	int		SetFrameCache(int channelId, int _cache);
	int		SetShownToScale(int channelId, int ShownToScale);
	int		SetDecodeType(int channelId, int _decodeKeyframeOnly);
	int		SetRenderRect(int channelId, LPRECT lpSrcRect);
	int		DrawLine(int channelId, LPRECT lpRect);
	int		SetDragStartPoint(int channelId, POINT pt);
	int		SetDragEndPoint(int channelId, POINT pt);
	int		ResetDragPoint(int channelId);
	int		AddBuoy(int channelId, D3D9_BUOY buoy);
	int		DoMouseEvent(int channelId, Render_MouseEventType eType, RECT rcWnd, char* sRetValue);

	//同一时间只支持一路声音播放
	int		PlaySound(int channelId);
	int		StopSound();

	int		StartManuRecording(int channelId);
	int		StopManuRecording(int channelId);

	int		SetManuRecordPath(int channelId, const char* recordPath);
	int		SetManuPicShotPath(int channelId, const char* shotPath);

	int		StartManuPicShot(int channelId);//pThread->manuScreenshot
	int		StopManuPicShot(int channelId);

	static LPTHREAD_START_ROUTINE __stdcall _lpDecodeThread( LPVOID _pParam );
	static LPTHREAD_START_ROUTINE __stdcall _lpDisplayThread( LPVOID _pParam );
	static LPTHREAD_START_ROUTINE __stdcall _lpPhotoShotThread( LPVOID _pParam );

	int		ProcessData(int _chid, int mediatype, char *pbuf, SKEYE_FRAME_INFO *frameinfo);

protected:
	bool	GetD3DSupportFormat();			//获取D3D支持的格式
	void	CreatePlayThread(PLAY_THREAD_OBJ	*_pPlayThread);
	void	ClosePlayThread(PLAY_THREAD_OBJ		*_pPlayThread);

	int		SetAudioParams(unsigned int _channel, unsigned int _samplerate, unsigned int _bitpersample);
	void	ClearAllSoundData();
	void	Release();	
	
private:
	PLAY_THREAD_OBJ			*pRealtimePlayThread;		//实时播放线程
	AUDIO_PLAY_THREAD_OBJ	*pAudioPlayThread;			//音频播放线程
	CRITICAL_SECTION		crit;
	D3D_ADAPTER_T		d3dAdapter;
	HINSTANCE	_hDLL;//DLL句柄

};
extern CChannelManager	*pChannelManager;
